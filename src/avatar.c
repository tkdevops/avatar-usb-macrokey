/*
 * Status   : Beta ����ͧ - (Alpha !! ����ͧ����Ѻ�����Ũҡ UART ����� mouse ��� key)
 * Project  : USB Keyboard & Mouse Emulator for Gamer
 * Codename : AVATAR Key Winner
 * �������
 * - �Ѳ�ҵ�ͨҡ F4 Key (���� project���) �����ͧ�ҡ�ѹ�������ö��Ǩ�Ң�Ҵ˹��¤����ӷ��������� port ���麹 WinAVR
 * - �������硢ͧ Keylife ����ö���� 4-8 ���� �����˹��¤����Ӣ������ LCD, �ҤҶ١�鹢�§������� ���Դ�ҡ
 * - �����ҹ��� key �Өҡ MCU �ͧ ����ͧ�ͤ���觨ҡ PC �������ö�Ѻ�� (code ��ǹ�����������)
 *   ����������´���礷��ӧҹ�ѹ�ѹ�� \WorkProject\MCU8051\Arduino\MyProjects\DotAKey
 *
 * [�Ӥѭ����ӴѺ ��Ф�͸Ժ��]
 * x- ���˹�ǧ����/��Ǩ�ͺ���� ���дѺ ms �� ���ͺ���� �Դ�Դ led
 * x- implement ultimate keyboard
 * x- ����ö��駡��������л�����ͧ�������仡������ҷ���������
 *      x- ���ͺ speed ������Ƿ���ش��������ö˹�ǧ�� ������ջѭ�� (��й���� 20/2/10 (debounce/repeat/released))
 * x- ��觷ӧҹ�� macrokey �� ���§����ӴѺ�� (��������´����ͧ keyboard ��� ultimatekey.c)
 * x- ��Ѻ����¹���������ͧ����� (�Ѻ serial �� save ŧ eeprom)
 * - ��駪��� ��Ф�� Device ID �� (��Ŵ�ҡ eeprom ���Ǥ��� initial USB �� Shadow Keyboard ����ó�Ẻ)
 *
 *
 * BUG !!!
 * - 25/1/2553 �ҡ����ա�����º USB Serial �ѹ�Ъ��ŧ 1 ��ҵ��... �ѧ��辺���˵�
 * ��������§ !! �Ҩ���ա���Ѻ�����������¹ŧ EEPROM ��ʹ����
 *
 * �����׺˹��
 * x- ��� interrupt INT1 �͡����
 * x- �����ǹ process IR �͡����
 * x- ��� code ��ǹ debug �͡�������
 * x- �Ѻ�����ŷҧ UART �����觵��价ҧ pc ������ ��� keyboard ��� mouse
 * x- �� mapkey, special key,macrokey ������
 * 
 * 2/5/2552 
 *  - ���ͧ optimize ���� ��Ҩ������ҳ����
 *
 * *** x- (��觷�跴�ͧ���� ��� work !!)
 *        implement ��ǹ i2c ���仴��� �������ö��觧ҹ�ҡ i2c ��
 *        �� code Wire �ͧ Arduino ���� folder Wire 
 *
 ******************************************************************/

/*
 * �ҡ�͡��� HID Keyboard �������ͧ make/break code �ͧ keyboard ����͹ ps/2
 * �ѹ�ШѴ����� host �����§���� report key � buffer �͡���� host
 * 
 In the KeyScan(), you don't need to consider make/break of keys - it is handled by the host side.
 Just put the keycode(s) to the keycode array on the report, for the key(s) currently pushed down.
 The keycode array can hold up to 6 keys - 6 key rollover. When more than 6 keys are pushed simultaneously,
 fill the keycode array with ErrorRollOver (0x01) When no key is pushed, clear the array with 0.

 * �óն�ҡ���ҧ ���������ҧ , ���¡����͡ buffer key ������, ��ҡ����� ��������Ѻ���� - �͡� � buffer
 *

// �к�����ͧ infrared HID ��ǹ�ͧ keyboard ���ͧ�Ѻ buffer �� 5 byte ����������� code
// �ҡ project WISHABI ���繵�����ҧ�����͹�������ö�� buffer �ú 8 byte ��
//
/////////////////////////////////////////////////////////////////////////////////////////
//Defines usbHidReportDescriptor - the form of input buffer array to be sent to
//udbSetInterrupt.  The array will take the following form:
/////////////////////////////////////////////////////////////////////////////////////////
//For Keyboard Buffer (uC to PC)/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//[ 1 , B1 , B2 , B3 , B4 , B5 , B6 , B7]
//  |   |    |_________________________|
//  |   |                 |
//  |   |                 |____Keyboard buttons (0x00 to 0x65)
//  |   |______________________Modifier Condition Where Bit0:Left Ctrl
//  |                                              Bit1:Left Shift
//  |                                              Bit2:Left Alt
//  |                                              Bit3:Left GUI (Windows Key)
//  |                                              Bit4:Right Ctrl
//  |                                              Bit5:Right Shift
//  |                                              Bit6:Right Alt
//  |                                              Bit7:Right GUI (Windows Key)
//  |__________________________Report ID = 1 for keyboard (2 for mouse)
/////////////////////////////////////////////////////////////////////////////////////////
//For Keyboard Buffer (PC to uC)/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//[ 1 , B1]
//  |   |
//  |   |
//  |   |
//  |   |______________________PC LED Conditions Where Bit0:Num Lock LED
//  |                                                  Bit1:Caps Lock LED
//  |                                                  Bit2:Scroll Lock LED
//  |                                                  Bit3:Compose LED
//  |                                                  Bit4:Kana LED
//  |                                                  Bit5:N/A
//  |                                                  Bit6:N/A
//  |                                                  Bit7:N/A
//  |__________________________Report ID = 1 for keyboard (2 for mouse)
/////////////////////////////////////////////////////////////////////////////////////////
//For Mouse Buffer///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//[ 2 , B1 , B2 , B3 ]
//  |   |    |    |
//  |   |    |    |_______Cursor movement along the Y-Axis (-127 to 127)
//  |   |    |____________Cursor movement along the X-Axis (-127 to 127)
//  |   |_________________Mouse Button Conditions where Bit0:Button One (Left Click)
//  |                                                   Bit1:Button Two (Right Click)
//  |                                                   Bit2:Button Three (Middle Click)
//  |                                                   Bit3:N/A
//  |                                                   Bit4:N/A
//  |                                                   Bit5:N/A
//  |                                                   Bit6:N/A
//  |                                                   Bit7:N/A
//  |_____________________Report ID = 2 for mouse (1 for keyboard)
/////////////////////////////////////////////////////////////////////////////////////////

 
 �ʴ� Bit �ͧ LED output report
The following table represents the keyboard output report (1 byte). 
Bit Description 
0  NUM LOCK 
1  CAPS LOCK 
2  SCROLL LOCK 
3  COMPOSE 
4  KANA 
5 to 7  CONSTANT 
  
The LEDs are absolute output items. This means that the state of each LED 
must be included in output reports (0 = off, 1 = on). Relative items would permit 
reports that affect only selected controls (0 = no change, 1= change). 
 

 ******************************************************************************************
 * ����Ѻ mouse ���ա���觢����� 3 byte ���������������� 4 byte �ѧ��� (㹷����������� 4 byte ���� 3 byte)
 * Ẻ 4 byte
 * 0xXX 0xXX 0xXX 0xXX
 *   ^    ^    ^    ^
 *   |    |    |    |
 *   |    |    |    +-- Fourth byte, for mouse's wheel(not used in this project)
 *   |    |    |        0x01 - wheel UP
 *   |    |    |        0xFF - wheel DOWN
 *   |    |    |        
 *   |    |    +------- Y axis (values depends on mouse's speed)
 *   |    |             0x81 .. 0x00 : -127 ..   0 : UP
 *   |    |             0x00 .. 0x7f :    0 .. 127 : DOWN
 *   |    |             
 *   |    +------------ X axis (values depends on mouse's speed)
 *   |                  0x81 .. 0x00 : -127 ..   0 : LEFT
 *   |                  0x00 .. 0x7f :    0 .. 127 : RIGHT
 *   |                  
 *   +----------------- mouse buttons states
 *                      r r r r r W R L, where:
 *                      r - reserved (depends on device type?)
 *                      W - wheel pressed (my mouse's wheel is a button too)
 *                      R - right button
 *                      L - left button
*/

/* Name: infrahid.c
 * Project: InfraHID; CIR to HID mouse/keyboard converter
 * Author: Alex Badea
 * Creation Date: 2007-01-14
 * Tabsize: 8
 * Copyright: (c) 2007 Alex Badea <vamposdecampos@gmail.com>
 * License: Proprietary, free under certain conditions. See Documentation.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <inttypes.h>

// #define DEBUG_LEVEL 1  // 任�С�ȷ�� Makefile �� parameter

#include "usbdrv.h"
#include "oddebug.h"

#define nop()	__asm__ __volatile__ ("nop");
#define reset()	__asm__ __volatile__ ("rjmp 0");

#define led_on()	    do { PORTB |= _BV(5);   } while (0)
#define led_off()	    do { PORTB &= ~_BV(5);  } while (0)
#define led_toggle()	do { PORTB ^= _BV(5);   } while (0)

#define ABS(x)	((x) < 0 ? -(x) : (x))

// �������� USART
#define BAUD_RATE			19200 // 38400

// ���˹�������� EEProm ����� �红�����
// ����纨��繢����ŷ�����������§�ѹ� �֧����ա�û�С�ȵ�������
#define EEPROM_ADDRESS_START    5       // ������鹷����˹觷�� 5 �����������ҵ��˹� 0 ��������º���
#define EEPROM_SIZE             480     // ��Ҵ�٧�ش������ŧ�
#define MAX_ADDRESS_EEPROM      (EEPROM_SIZE+EEPROM_ADDRESS_START)  // ����٧�ش�ͧ���˹� address ���������ԧ

/* ------------------ GLOBAL VARIABLE -------------------------------------- */
extern struct DirectMouse
{
    uchar workingFlag;              // ����� true �зӧҹ� loop mouse ��¹͡��͹ (��Ѻ����觡�˹� x,y mouse)
    uint16_t XStepReset;            // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0
    uint16_t YStepReset;            // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0

    uint16_t XStepTarget;           // �Ѻ�ͺ��ҷ��������Ƿ������� ������� X,Y
    uint16_t YStepTarget;           // �Ѻ�ͺ��ҷ��������Ƿ������� ������� X,Y
    
    uint16_t XTarget;               // ���˹� x ���·ҧ
    uint16_t YTarget;               // ���˹� y ���·ҧ
}directMouse;  // ��Ѻ����� ��˹����˹� mouse

// function �������ҹ/��¹ eeprom
extern uchar eeprom_read_byte(uint16_t uiAddress);
extern void eeprom_write_byte(uint16_t uiAddress, uchar ucData);
extern uchar eeprom_busy();
uchar eeprom_read_byte_with_flag(uint16_t uiAddress,uchar* success_flag);

// ������Ңͧ�к�(ms) ����ͷӧҹ�ҡ interval ���� 0
extern unsigned long sys_millis;        // �纤�����Ңͧ�к�
extern uchar tx_kbd;                    // ���Ǩ��͹��
extern uchar tx_mouse;                  // ���Ǩ��͹��
extern uchar inputBuffer1[8];           // = { 1, 0,0,0,0,0,0,0, };
extern uchar inputBuffer2[4];           // = { 2, 0,0,0, };
//extern uchar IsNumlockOn;               // �纤�һ��� numlock ��� on ���� off

// initial Global variable 
unsigned long sys_millis = 0;           // reset system time
uchar tx_kbd = 0;
uchar tx_mouse = 0;
uchar inputBuffer1[8] = { 1, 0,0,0,0,0,0,0, };
uchar inputBuffer2[4] = { 2, 0,0,0, };
struct DirectMouse directMouse;
//uchar expectReport = 0;               // ���� report �͹��Ǩʶҹ� numlock
//uchar IsNumlockOn = 0;

#include "ultimatekey.c"

/* ------------------------------------------------------------------------- */

/* ----------------------- �������ǹ��û�С�Ȥ�� USB --------------------------- */

static const char hidReportDescriptor0[] PROGMEM = {
	/* partial keyboard */ // �ͧ���
//	0x05, 0x01,	/* Usage Page (Generic Desktop), */
//	0x09, 0x06,	/* Usage (Keyboard), */
//	0xA1, 0x01,	/* Collection (Application), */
//	0x85, 0x01,		/* Report Id (1) */
//	0x95, 0x04,		/* Report Count (4), */
//	0x75, 0x08,		/* Report Size (8), */
//	0x15, 0x00,		/* Logical Minimum (0), */
//	0x25, 0x65,		/* Logical Maximum(101), */
//  0x05, 0x07,		/* Usage Page (Key Codes), */
//	0x19, 0x00,		/* Usage Minimum (0), */
//	0x29, 0x65,		/* Usage Maximum (101), */
//	0x81, 0x00,		/* Input (Data, Array),               ;Key arrays (4 bytes) */
//	0xC0,		/* End Collection */

    /* mouse */
//	0x05, 0x01,	/* Usage Page (Generic Desktop), */
//	0x09, 0x02,	/* Usage (Mouse), */
//	0xA1, 0x01,	/* Collection (Application), */
//	0x09, 0x01,	/*   Usage (Pointer), */
//	0xA1, 0x00,	/*   Collection (Physical), */
//	0x05, 0x09,		/* Usage Page (Buttons), */
//	0x19, 0x01,		/* Usage Minimum (01), */
//	0x29, 0x03,		/* Usage Maximun (03), */
//	0x15, 0x00,		/* Logical Minimum (0), */
//	0x25, 0x01,		/* Logical Maximum (1), */
//	0x85, 0x02,		/* Report Id (2) */
//	0x95, 0x03,		/* Report Count (3), */
//	0x75, 0x01,		/* Report Size (1), */
//	0x81, 0x02,		/* Input (Data, Variable, Absolute), ;3 button bits */
//	0x95, 0x01,		/* Report Count (1), */
//	0x75, 0x05,		/* Report Size (5), */
//	0x81, 0x01,		/* Input (Constant),  ;5 bit padding */ // project IRMouse �� 0x81, 0x03 = INPUT (Cnst,Var,Abs)
//	0x05, 0x01,		/* Usage Page (Generic Desktop), */
//	0x09, 0x30,		/* Usage (X), */
//	0x09, 0x31,		/* Usage (Y), */
//	0x15, 0x81,		/* Logical Minimum (-127), */
//	0x25, 0x7F,		/* Logical Maximum (127), */
//	0x75, 0x08,		/* Report Size (8), */
//	0x95, 0x02,		/* Report Count (2), */
//	0x81, 0x06,		/* Input (Data, Variable, Relative), ;2 position bytes (X & Y) */
//	0xC0,		/*   End Collection, */
//	0xC0,		/* End Collection */

/////////// ��� ����Ҩҡ WiSHABI project
// Keyboard
//	-------------------------------------------------------------------------
	0x05, 0x01,//	USAGE_PAGE (Generic Desktop)							|
	0x09, 0x06,//	USAGE (Keyboard)										|
	0xa1, 0x01,//	COLLECTION (Application)								|
//	-------------------------------------------------------------------------
	0x85, 0x01,//		Report Id (1)										|Byte 0 - Report ID = 1 for Keyboard
//	-------------------------------------------------------------------------
	0x05, 0x07,//		USAGE_PAGE (Keyboard)								|
	0x19, 0xe0,//		USAGE_MINIMUM (Keyboard LeftControl)				|Defines byte 1 form when byte
	0x29, 0xe7,//		USAGE_MAXIMUM (Keyboard Right GUI)					|0 is 1 (keyboard) for data reception.
	0x15, 0x00,//		LOGICAL_MINIMUM (0)									|Represents the state of each of the 8
	0x25, 0x01,//		LOGICAL_MAXIMUM (1)									|modifiers (ctrls, alts,
	0x75, 0x01,//		REPORT_SIZE (1)										|shifts, and guis)
	0x95, 0x08,//		REPORT_COUNT (8)									|
	0x81, 0x02,//		INPUT (Data,Var,Abs)								|
//	-------------------------------------------------------------------------
	0x95, 0x05,//		REPORT_COUNT (5)									|
	0x75, 0x01,//		REPORT_SIZE (1)										|Defines byte 1 form when byte
	0x05, 0x08,//		USAGE_PAGE (LEDs)									|0 is 1 (keyboard) for data transmission.
	0x19, 0x01,//		USAGE_MINIMUM (Num Lock)							|Represents the status of the PC's LEDs
	0x29, 0x05,//		USAGE_MAXIMUM (Kana)								|(Caps Lock, Num Lock etc).  5-bits.
	0x91, 0x02,//		OUTPUT (Data,Var,Abs)								|
//	-------------------------------------------------------------------------
	0x95, 0x01,//		REPORT_COUNT (1)									|
	0x75, 0x03,//		REPORT_SIZE (3)										|3-bits to pad previous section to a byte.
	0x91, 0x03,//		OUTPUT (Cnst,Var,Abs)								|
//	-------------------------------------------------------------------------
	0x95, 0x06,//		REPORT_COUNT (6)									|
	0x75, 0x08,//		REPORT_SIZE (8)										|Defines bytes 2-7 form when byte
	0x15, 0x00,//		LOGICAL_MINIMUM (0)									|0 is 1 (keyboard). Represents
	0x25, 0x65,//		LOGICAL_MAXIMUM (101)								|any of the regular keyboard
	0x05, 0x07,//		USAGE_PAGE (Keyboard)								|characters
	0x19, 0x00,//		USAGE_MINIMUM (Reserved (no event indicated))		|
	0x29, 0x65,//		USAGE_MAXIMUM (Keyboard Application)				|
	0x81, 0x00,//		INPUT (Data,Ary,Abs)								|
	0xc0,//			END_COLLECTION  										|
//	-------------------------------------------------------------------------

// Mouse 
//	-------------------------------------------------------------------------
	0x05, 0x01,//	Usage Page (Generic Desktop),							|
	0x09, 0x02,//	Usage (Mouse),											|Defines byte 1 form when byte
	0xA1, 0x01,//	Collection (Application),								|0 is 2 (mouse). Represents
	0x09, 0x01,//		Usage (Pointer)										|the condition of three mouse
	0xA1, 0x00,//		Collection (Physical),								|buttons (thus only 3 bits,
	0x85, 0x02,//			Report Id (2).									|
	0x05, 0x09,//			Usage Page (Buttons),							|remaining 5 bits padded as
	0x19, 0x01,//			Usage Minimum (01),								|constants by the second
	0x29, 0x03,//			Usage Maximun (03),								|"Input" line.
	0x15, 0x00,//			Logical Minimum (0),							|
	0x25, 0x01,//			Logical Maximum (1),							|
	0x95, 0x03,//			Report Count (3),								|
	0x75, 0x01,//			Report Size (1),								|
	0x81, 0x02,//			Input (Data, Variable, Absolute), ;3 button bits|
	0x95, 0x01,//			Report Count (1),								|
	0x75, 0x05,//			Report Size (5),								|
	0x81, 0x01,//			Input (Constant), ;5 bit padding				|
//	-------------------------------------------------------------------------
	0x05, 0x01,//			Usage Page (Generic Desktop),					|
	0x09, 0x30,//			Usage (X),										|Defines form of bytes 2
	0x09, 0x31,//			Usage (Y),										|and 3 when byte 0 is 2
	0x15, 0x81,//			Logical Minimum (-127),							|(mouse). Represents the motion
	0x25, 0x7F,//			Logical Maximum (127),							|increment of the cursor in the
	0x75, 0x08,//			Report Size (8),								|x-axis (byte 2) or y axis
	0x95, 0x02,//			Report Count (2),								|(byte 3).
	0x81, 0x06,//			Input (Data, Var, Rel),2 position bytes (X & Y)	|
//	-------------------------------------------------------------------------
	0xC0,//				End Collection,										|
	0xC0,//			End Collection											|
//	-------------------------------------------------------------------------
};


static const char configDescriptor[] PROGMEM = {	/* USB configuration descriptor */
	9,			/* sizeof(usbDescriptorConfiguration): length of descriptor in bytes */
	USBDESCR_CONFIG,	/* descriptor type */
	9 + 1 * (9 + 9 + 7), 0,
	/* total length of data returned (including inlined descriptors) */
	1,			/* number of interfaces in this configuration */
	1,			/* index of this configuration */
	0,			/* configuration name string index */
	USBATTR_BUSPOWER,	/* attributes */
	USB_CFG_MAX_BUS_POWER / 2,	/* max USB current in 2mA units */

/* --- interface 0 --- */
	9,			/* sizeof(usbDescrInterface): length of descriptor in bytes */
	USBDESCR_INTERFACE,	/* descriptor type */
	0,			/* index of this interface */
	0,			/* alternate setting for this interface */
	1,			/* endpoints excl 0: number of endpoint descriptors to follow */
	3,			/* 3=HID class */
	0,			/* subclass: 0=none, 1=boot */
	0,			/* protocol: 0=none, 1=keyboard, 2=mouse */
	0,			/* string index for interface */

	/* HID descriptor */
	9,			/* sizeof(usbDescrHID): length of descriptor in bytes */
	USBDESCR_HID,		/* descriptor type: HID */
	0x01, 0x01,		/* BCD representation of HID version */
	0x00,			/* target country code */
	0x01,			/* number of HID Report (or other HID class) Descriptor infos to follow */
	0x22,			/* descriptor type: report */
	sizeof(hidReportDescriptor0), 0,	/* total length of report descriptor */

	/* endpoint descriptor for endpoint 1 */
	7,			/* sizeof(usbDescrEndpoint) */
	USBDESCR_ENDPOINT,	/* descriptor type = endpoint */
	0x81,			/* IN endpoint number 1 */
	0x03,			/* attrib: Interrupt endpoint */
	8, 0,			/* maximum packet size */
	USB_CFG_INTR_POLL_INTERVAL,	/* in ms */
};

uchar usbFunctionDescriptor(usbRequest_t * rq)
{
	uchar *p = 0, len = 0;

	DBG1(0xa8, &rq->wValue.bytes[1], 1);

#if 0	/* default descriptor from driver */
	if (rq->wValue.bytes[1] == USBDESCR_DEVICE) {
		p = (uchar *) deviceDescriptor;
		len = sizeof(deviceDescriptor);
	} else
#endif
	if (rq->wValue.bytes[1] == USBDESCR_CONFIG) {
		p = (uchar *) configDescriptor;
		len = sizeof(configDescriptor);
	} else if (rq->wValue.bytes[1] == USBDESCR_HID) {
		p = (uchar *) (configDescriptor + 18);
		len = 9;
	} else if (rq->wValue.bytes[1] == USBDESCR_HID_REPORT) {
		p = (uchar *) hidReportDescriptor0;
		len = sizeof(hidReportDescriptor0);
	}
	usbMsgPtr = p;
	return len;
}

/* ���ͧ������� work 
// The write function is called when LEDs should be set
// ��㹡����ҹ��� LED ���ѧ������� � �͹���
// expectReport ��͵���÷�����ͧ
// uchar expectReport = 0;
#define LED_NUM     0x01
#define LED_CAPS    0x02
#define LED_SCROLL  0x04
#define LED_COMPOSE 0x08
#define LED_KANA    0x10
// USB description config copy �Ҩҡ Wishabi �ѧ��� �ç����ͧ����¹仵�� Wishabi
uchar usbFunctionWrite(uchar *data, uchar len)
{
	if ((expectReport)&&(len==2))
	{
        uchar LEDstate = data[1];     // Get the state of all 5 LEDs 
        IsNumlockOn = LEDstate & LED_NUM;
    }
    expectReport = 0;
    return 0x01;
}
*/

/* ----------------------- ����ǹ��û�С�Ȥ�� USB ----------------------------- */

/* ---------------------- �������ǹ�Ѵ��� EEprom -------------------------------- */
// return >0 (true) ����ѧ��ҹ��¹����
// return 0  (false)�����ҧ����
uchar eeprom_busy()
{
    if (EECR&(1<<EEWE))
    {
        led_on();
        return true;
    }
    led_off();
    return false;

    // return EECR&(1<<EEWE);
}
// ��ҹ��Ҩҡ eeprom
uchar eeprom_read_byte(uint16_t uiAddress)
{
    uchar successFlag = false;
    uchar eepromRead = '\0';
    do{
        //wdt_reset(); // reset watchdog
        eepromRead = eeprom_read_byte_with_flag(uiAddress, &successFlag);
    }while( !successFlag );
    return eepromRead;
}

// �׹����������ҹ eeprom ��駹���ͧ�� success_flag ��������� true , ����� false ��ҷ��׹���� 0 ����
uchar eeprom_read_byte_with_flag(uint16_t uiAddress,uchar* success_flag)
{
    // ��� eeprom ���ѧ�ӧҹ����
    if(eeprom_busy()) 
    {
        *success_flag = false;
        return 0;
    }

    //��˹� address ������ҹ
    EEAR = uiAddress;
    // ����������ҹ
    EECR |= (1<<EERE);
    *success_flag = true;
    return EEDR;
}

// ��¹ eeprom ������͡����¹������� (����� polling ��Ǩ)
void eeprom_write_byte(uint16_t uiAddress, uchar ucData)
{
    // ��� eeprom ���ѧ�ӧҹ����
    if(eeprom_busy()) return;

    //��˹� address ������ҹ
    EEAR = uiAddress;
    EEDR = ucData;

    EECR |= (1<<EEMWE);
    // ����������¹
    EECR |= (1<<EEWE);
}
/* ----------------------- ����ǹ�Ѵ��� EEprom -------------------------------- */

/* ---------------------- �������ǹ code �к� �ͧ��� ---------------------------- */
// �ͧ���
//static uchar inputBuffer1[5] = { 1, 0,0,0,0, };		/* kbd */
//static uchar inputBuffer2[4] = { 2, 0,0,0, };		/* mouse */
// �ͧ���� ��С���� extern �ç header

static uchar idleRate = 0;							/* in 4 ms units */

uchar usbFunctionSetup(uchar data[8])
{
	usbRequest_t *rq = (void *) data;

	DBG1(0xa0, data, 8);

	if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {	/* class request type */
		if (rq->bRequest == USBRQ_HID_GET_REPORT) {					// ��Ǩ���� HID
			/* wValue: ReportType (highbyte), ReportID (lowbyte) */
			if (rq->wValue.bytes[0] == 1) {							// �� keyboard
				usbMsgPtr = inputBuffer1;
				return sizeof(inputBuffer1);
			} else if (rq->wValue.bytes[0] == 2) {					// �� mouse
				usbMsgPtr = inputBuffer2;
				return sizeof(inputBuffer2);
			}
			return 0;
		} else if (rq->bRequest == USBRQ_HID_SET_REPORT) {
			/* wValue: ReportType (highbyte), ReportID (lowbyte) */
			/* we have no output/feature reports */
            return 0;
            // ���ͧ������� work!!
            // �ç����繡���Ѻʶҹ� LED �Ѩ�غѹ (�ҡ project Spiffcord)
            /*if (rq->wLength.word == 2)// We expect two byte reports.  B1: Report ID, B2: Status of Keyboard LEDs
			{
				expectReport=1;
				return 0xFF; // Call usbFunctionWrite with data 
			}*/
		} else if (rq->bRequest == USBRQ_HID_GET_IDLE) {
			usbMsgPtr = &idleRate;
			return 1;
		} else if (rq->bRequest == USBRQ_HID_SET_IDLE) {
			idleRate = rq->wValue.bytes[1];
		}
		return 0;
	}

	return 0;
}


/* "more thorough" reset: loop 4 times outputting SE0 then idle.
 * This works around some intermitent connection problems. 
 * �Ҵ����繡��˹�ǧ������� reset USB ���кҧ���駡�����º usb ���ʹԷ �Ҩ���ջѭ��
 * �·ӧҹ�����Ѻ WatchDog Timer
 */

static void usbReset(void)
{
	uchar i, j, k;

	for (k = 0; k < 8; k++) {
		/* on even iterations, output SE0 */
		if (!(k & 1)) {
			PORTD &= ~USBMASK;	/* no pullups on USB pins */
			DDRD |= USBMASK;	/* output SE0 for USB reset */
		} else {
			DDRD &= ~USBMASK;	/* set USB data as inputs */
		}

		/* delay */
		j = 0;
		while (--j) {		/* USB Reset by device only required on Watchdog Reset */
			i = 0;
			while (--i) {	/* delay >10ms for USB reset */
				nop();
				nop();
			}
		}
	}
}

/* -------------------- ����ǹ code �к� �ͧ��� ------------------------------------------- */

/* -------------------- ��ǹ interrupt USART ---------------------------------------------- */
// funtion �����㹧ҹ���
void uasrtSetModeFromReceivedChar();
void usartClearCounter();
void usartKeyboardAndMouseControl();
void usartUpdateEEProm();
void usartReturnEEProm();
extern void usartPutchar(uchar ch);
extern void usartSendWord(int16_t wd);

// ����õ�ҧ� �����㹧ҹ���
// �к�������ҧ�
static uchar usart_mode = 'X';
static uchar usart_receiveBytes = 0;            // ��Ѻ�����Ŷ���� keyboard �йѺ 7 byte , mouse �Ѻ 3 byte
static uchar usart_counterBytes = 0;	        // array index counter
static uchar usart_receivedChar = '\0';         // �纤�ҵ���÷����ҡ usart
static uchar usart_buffer_empty = true;         // ����բ������ѧ����������� ���� true

// ����� update
static uint16_t usart_updateSize = 0;           // �纨ӹǹ byte ���е�ͧ update
static uint16_t usart_updateSizeCounter = 0;    // ��ǹѺ�ӹǹ byte ����Ѻ������
static uchar    usart_updatingFlag = false;     // ����� true ���������� update �¨����¡ function updateEEprom
                                                // ����纤ú�� �׹����� "false" ����ش�͡�ҡ loop ���      
// ��觷���ͧ������ 
// - �к� parity
// - command ����ҡ���ҹ��
//
// �Ѻ�����Ũҡ Serial ���ǨѴ���§ŧ buffer �͡����� PC
// Protocal ����� ���ҵ���� 200 (0xc8) ����
// - 200 �觢����� keyboard ���ǵ������ ������ 7 bytes �� byte �á�� modifiers 
// - 201 �觢����� mouse ���ǵ������ ������ 3 bytes
// - 202 ��Ǩ����ѧ������ӧҹ����������� �¨еͺ��Ѻ��� "A" (Acknowledge) (�ѧ��� implement)
// - 203 ���緤������������ �¨еͺ��Ѻ��� "A" (Acknowledge) ���� reset ������������ (�ѧ��� implement)
// - 204 start ��� update eeprom ������������ update eeprom ������¢�Ҵ����ա 4 byte (����� high byte) �������Ѻ������
// - 205 �׹����˹��¤����� eeprom �Ѩ�غѹ �������仵�Ǩ�ͺ
// mode ��ҧ� K=Keyboard, M=Mouse, X=Empty, U=Update, R=Return EEprom
// ����ա���к��������� �����ŷ����ż�ҹ�ҡ������� buffer ���ҧ����

#define UART_KEYBOARD_CMD   200     // �觤�������ǵ�����¢����� 7 byte (byte �á�� modifiers)
#define UART_MOUSE_CMD      201     // �觤�������ǵ�����¢����� 3 byte
#define UART_READY_CMD      202
#define UART_RESET_CMD      203
#define UART_UPDATE_CMD     204     // �觤�������ǵ������ size �ա 2 byte �����觢������������
#define UART_RETURN_CMD     205     // �觤׹��� eeprom �º͡�ӹǹ size (2byte) ���Ǥ����觢����� �ҡ EEprom

// send byte to usart
void usartPutchar(uchar ch)
{
    loop_until_bit_is_set(UCSRA,UDRE);
    UDR = ch;
}

// �觢����� 2 byte (��� protocal �觢�Ҵ��Ѻ� ���� byte �٧��͹)
void usartSendWord(int16_t wd)
{
    usartPutchar( (uchar)(wd >> 8) );
    usartPutchar( (uchar)(wd) );
}

// ��駤�� mode �ҡ��ҷ���Ѻ�ҷҧ UART
void uasrtSetModeFromReceivedChar()
{
    if (usart_receivedChar == UART_KEYBOARD_CMD)
    {
        usart_mode = 'K';
        usart_receiveBytes = 7;       // �� 7 bytes                      
    }        
    else if (usart_receivedChar == UART_MOUSE_CMD) // mouse mode
    {
        usart_mode = 'M';
        usart_receiveBytes = 3;       // �� 3 bytes                      
    }
        /*case UART_READY_CMD: // test ����ѧ�ӧҹ�������
        {
            // �ѧ����� implement
            break; 
        }
        case UART_RESET_CMD: // reset ���������Ѻ����
        {
            // �ѧ����� implement
            break; 
        }*/
    else if (usart_receivedChar == UART_UPDATE_CMD) // �Ѻ�������� update
    {
        usart_mode = 'U'; // Update
        usart_receiveBytes = 2;   // �� 2 bytes 㹡���红�Ҵ
    }
    else if (usart_receivedChar == UART_RETURN_CMD) // �觢����� eeprom ��Ѻ仵�Ǩ
    {
        // ����ͧ setmode �ӧҹ�����
        usartReturnEEProm();
    }
    usart_counterBytes = 0;
}

// clear usart counter
void usartClearCounter()
{
    usart_counterBytes = 0;
    usart_receiveBytes = 0;
    usart_mode = 'X';
    usart_buffer_empty = true;
}

// �Ѻ�����������ŧ buffer ������� keyboard mouse �ӧҹ
void usartKeyboardAndMouseControl()
{
    // ����к��������ǡ����Ҩ��ŧ��� keyboard ���� mouse
    if (usart_mode == 'K')
    {
        inputBuffer1[++usart_counterBytes] = usart_receivedChar;
        if (usart_counterBytes >= usart_receiveBytes)
            tx_kbd = 1;
    }
    // mouse
    else // if (usart_mode == 'M') ����ͧ��Ǩ ���е�Ǩ��������� ��Ҷ������� K ���� M ��͹ (�����Ѵ)
    {
        inputBuffer2[++usart_counterBytes] = usart_receivedChar;
        if (usart_counterBytes >= usart_receiveBytes)
            tx_mouse = 1;
    }
    // ��Ǩ����Ѻ�����Ťú����л����������ѧ
    // �ú���� clear ���
    if (usart_counterBytes >= usart_receiveBytes)
    {
        usartClearCounter();
        // led_on(); // �ʴ�ʶҹ��ѡ˹���
    }
}

// �Ѻ����������ŧ��� eeprom
void usartUpdateEEProm()
{
    // ����� mode update ����Ѻ�����Ţ�Ҵ��� �ú���ǡ����������Ѻ��Ҩҡ uart ��纷�� RAM(������仺ѹ�֡ŧeeprom �ա��)
    if (usart_updatingFlag)
    {
        // �Ѻ������������save ŧ eeprom Ẻ���Դ loop
        if (eeprom_busy()) return; // ��������ҧ ������ͧ��¹
        if (usart_buffer_empty) return; // �������բ����� ������ͧ��¹

        eeprom_write_byte(usart_updateSizeCounter + EEPROM_ADDRESS_START,usart_receivedChar);
        usart_buffer_empty = true;
        usart_updateSizeCounter++;
        if (usart_updateSizeCounter >= usart_updateSize) // �Ѻ�ú������� clear ��ҡ�Ѻ����������
        {
            usart_updatingFlag = false;
            usart_updateSize = 0;
            usart_updateSizeCounter = 0;
            usartClearCounter();
        }
        // �觢�����仺͡��� ��ҧ���� �������Ѻ byte ����
        usartPutchar('S'); // success
    }
    else
    {
        // ����ѧ�����颹Ҵ size ����Ѻ���ú��� 2 byte ��͹ ������ҡ high byte
        usart_updateSize |= usart_receivedChar;
        usart_counterBytes++;
        if (usart_counterBytes == 1) // ��ͧ�Ѻ 2 byte ���Ъ�ǧ���� ��ҹѺ������ byte �á��ͧ����͹ bit
        {
            usart_updateSize <<= 8;
        }
        // ����ͤú��������������� update EEprom
        if (usart_counterBytes >= usart_receiveBytes)
        {
            usart_buffer_empty = true; // ** �������պ�÷Ѵ�������� byte �á�мԴ��Ҵ
            usart_updatingFlag = true;
            // ��Ң�Ҵ�Թ���� EEPROM ����������
            if (usart_updateSize > EEPROM_SIZE)
                usart_updateSize = EEPROM_SIZE;
        }
    }
}

// �觡�Ѻ��� eeprom �������仵�Ǩ�ͺ
// *** ����������ӧҹẺ���Դ�ٻ �ç��������
// 㹡óն���բ����ŷ���ͧ������ "�Ҩ��" �繻ѭ�� ���� usb ����� polling update ������
// �跴�ͺ���Ƿ�� 512 bytes work !!
void usartReturnEEProm()
{
    // ������ҧ��� disable this interrupt 
    // UCSRB &= ~_BV(RXCIE);
    // ������ҧ��� reenable usart rx interrupt 
    // UCSRB |= _BV(RXCIE);

    // ��� protocal �觢�Ҵ��Ѻ� ���� byte �٧��͹
    usartSendWord(EEPROM_SIZE);
    
    // ����ǹ loop �觢�����价�����
    uint16_t addr = EEPROM_ADDRESS_START;
    for (; addr < MAX_ADDRESS_EEPROM; addr++)
        usartPutchar(eeprom_read_byte(addr));

    // �ӧҹ���稡� clear
    usartClearCounter();
}


// ��駤�� USART
void UART_init()
{
    /*** UART init ***/
    // can receive and transmit, but receive use interrupt
    UCSRB = _BV(RXEN) | _BV(TXEN) | _BV(RXCIE);
    // asynchronous mode, 8N1
    UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);
    // baud rate
    UBRRH = (unsigned char)(((F_CPU)/((BAUD_RATE)*16l)-1)>>8);
    UBRRL = (unsigned char) ((F_CPU)/((BAUD_RATE)*16l)-1);
}

// ISR(USART_RX_vect) <- �� vector ������� warning
// ��Ǩ interrupt vector ��ҡ C:\WinAVR-20090313\avr\include\avr\iomxxx.h
// �红�����ŧ buffer ���ҧ������� �������������������ǹ�����żŤ�����ա�ͺ
ISR(USART_RXC_vect)
{
    usart_receivedChar = UDR;
    usart_buffer_empty = false;         // �բ���������
    // ����ѧ����к� mode
    if (usart_mode == 'X')
    {
        uasrtSetModeFromReceivedChar();
    }
    // ����� mode �� keyboard ���� mouse
    else if (usart_mode == 'K' || usart_mode == 'M')
    {
        usartKeyboardAndMouseControl();  
    }
    // mode update
    else if (usart_mode == 'U') 
    {
        usartUpdateEEProm();
    }
}

/* -------------------- ��ǹ interrupt USART ---------------------------------------------- */

/* ---------------------- �������ǹ����ǡѺ project (����к�) ------------------------- */

/* Keyboard usage values, see usb.org's HID-usage-tables document, chapter
 * 10 Keyboard/Keypad Page for more codes.
 * ���ʹ�� Appendic C scancode.doc �ͧ Microsoft http://www.microsoft.com/whdc/archive/scancode.mspx#ENB
 */

#define MOD_CONTROL_LEFT    (1<<0)
#define MOD_SHIFT_LEFT      (1<<1)
#define MOD_ALT_LEFT        (1<<2)
#define MOD_GUI_LEFT        (1<<3)
#define MOD_CONTROL_RIGHT   (1<<4)
#define MOD_SHIFT_RIGHT     (1<<5)
#define MOD_ALT_RIGHT       (1<<6)
#define MOD_GUI_RIGHT       (1<<7)

#define KEY_NONE         0
#define KEY_ROLL_ERROR   1
#define KEY_POST_FAIL    2
#define KEY_UNDEFINED_ERROR 3

#define KEY_A            4
#define KEY_B            5
#define KEY_C            6
#define KEY_D            7
#define KEY_E            8
#define KEY_F            9
#define KEY_G           10
#define KEY_H           11
#define KEY_I           12
#define KEY_J           13
#define KEY_K           14
#define KEY_L           15
#define KEY_M           16
#define KEY_N           17
#define KEY_O           18
#define KEY_P           19
#define KEY_Q           20
#define KEY_R           21
#define KEY_S           22
#define KEY_T           23
#define KEY_U           24
#define KEY_V           25
#define KEY_W           26
#define KEY_X           27
#define KEY_Y           28
#define KEY_Z           29

#define KEY_1           30
#define KEY_2           31
#define KEY_3           32
#define KEY_4           33
#define KEY_5           34
#define KEY_6           35
#define KEY_7           36
#define KEY_8           37
#define KEY_9           38
#define KEY_0           39

#define KEY_ENTER       40
#define KEY_ESCAPE      41
#define KEY_BACKSPACE   42
#define KEY_TAB         43
#define KEY_SPACE       44
#define KEY_DASH        45
#define KEY_EQUALS      46
#define KEY_LPAREN      47
#define KEY_RPAREN      48
#define KEY_BACKSLASH   49

#define KEY_NON_US_HASH 50
#define KEY_SEMICOLON   51
#define KEY_QUOTE       52
#define KEY_TILDE       53
#define KEY_COMMA       54
#define KEY_DOT         55
#define KEY_SLASH       56

#define KEY_CAPS_LOCK   57

#define KEY_F1          58
#define KEY_F2          59
#define KEY_F3          60
#define KEY_F4          61
#define KEY_F5          62
#define KEY_F6          63
#define KEY_F7          64
#define KEY_F8          65
#define KEY_F9          66
#define KEY_F10         67
#define KEY_F11         68
#define KEY_F12         69

#define KEY_PRINT_SCREEN 70
#define KEY_SCROLL_LOCK 71
#define KEY_PAUSE       72
#define KEY_INSERT      73
#define KEY_HOME        74
#define KEY_PAGE_UP     75
#define KEY_DELETE      76
#define KEY_END         77
#define KEY_PAGE_DOWN   78

#define KEY_RIGHT_ARROW 79
#define KEY_LEFT_ARROW  80
#define KEY_DOWN_ARROW  81
#define KEY_UP_ARROW    82

#define KEY_NUM_LOCK    83

#define KEY_NUM_DIV     84
#define KEY_NUM_MUL     85
#define KEY_NUM_SUB     86
#define KEY_NUM_ADD     87

#define KEY_NUM_ENTER   88
#define KEY_NUM_1       89
#define KEY_NUM_2       90
#define KEY_NUM_3       91
#define KEY_NUM_4       92
#define KEY_NUM_5       93
#define KEY_NUM_6       94
#define KEY_NUM_7       95
#define KEY_NUM_8       96
#define KEY_NUM_9       97
#define KEY_NUM_0       98
#define KEY_NUM_DOT     99


void directMouseReset()
{
    directMouse.workingFlag = false;    // ����� true �зӧҹ� loop mouse ��¹͡��͹ (��Ѻ����觡�˹� x,y mouse)
    directMouse.XStepReset = 0;         // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0
    directMouse.YStepReset = 0;         // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0
    directMouse.XStepTarget = 0;        // �Ѻ�ͺ��ҷ��������Ƿ������� ������� X,Y
    directMouse.YStepTarget = 0;        // �Ѻ�ͺ��ҷ��������Ƿ������� ������� X,Y    
    directMouse.XTarget = 0;            // ���˹� x ���·ҧ
    directMouse.YTarget = 0;            // ���˹� y ���·ҧ
}

void hid_mouse_clear(void)
{
    inputBuffer2[1] = 0;	// mouse buttons
	inputBuffer2[2] = 0;	// X axis delta
	inputBuffer2[3] = 0;	// Y axis delta
}

void hid_keyboard_clear(void)
{
    inputBuffer1[1] = 0;	// modifiers
	inputBuffer1[2] = 0;	// key
	inputBuffer1[3] = 0;	// key
	inputBuffer1[4] = 0;	// key
	inputBuffer1[5] = 0;	// key
	inputBuffer1[6] = 0;	// key
	inputBuffer1[7] = 0;	// key
}

void processDirectMouse()
{
    // ����觢��������������觢�����
    if(!tx_mouse)
    {
        // �������� step ��� reset ��� 0,0
        if (directMouse.XStepReset >0 || directMouse.YStepReset >0)
        {        
            // Ŵ����������� �����Ҩ��� 0,0 (�����ش ���ش)
            if (directMouse.XStepReset >0) // X axis delta
            {
                inputBuffer2[2] = -120;	
                directMouse.XStepReset--;
            }
            if (directMouse.YStepReset >0) // Y axis delta
            {
                inputBuffer2[3] = -120;
                directMouse.YStepReset--;
            }                   
        }
        else // �������� step ������� x,y 
        {
            if (directMouse.XStepTarget >0 || directMouse.YStepTarget >0)
            {
                if (directMouse.XStepTarget >0)
                {
                    inputBuffer2[2] = 120;
                    directMouse.XStepTarget--;
                }
                if (directMouse.YStepTarget >0)
                {
                    inputBuffer2[3] = 120;
                    directMouse.YStepTarget--;
                }
            }
            // �������͹���ú���� 
            else if (directMouse.XStepTarget == 0 && directMouse.YStepTarget == 0)
            {
                // ��˹���Ҥ����ش����
                inputBuffer2[2] = directMouse.XTarget;
                inputBuffer2[3] = directMouse.YTarget;
                // ������� reset directMouse ���
                directMouseReset();
            }
        } 
        tx_mouse = 1;
    }
}

/*// clear ��� HID (����͹��� unpress)
static void hid_clear(void)
{
    hid_mouse_clear();
    hid_keyboard_clear();
}*/


/* this may well be SIGNAL(SIG_OVERFLOW0), but we want it called at well-defined times */
// ����ѹ���ҡ���һ�С���� interrupt timer 0 SIGNAL(SING_OVERFLOW0)
// ���ͺ���� stable ����Ẻ interrupt timer �ҡ�
// �Ҵ��ҹ�Ҩ����繡�õ�Ǩ������Ҩ��������ѭ�ҳ unpress keyboard ���������
void idle_timer(void)
{		
	static uchar idleCounter = 0;
    /* idle reports, if requested */
    // ���˹�ǧ���Ңͧ�к� �����͡�Ҩҡ PC 㹡óշ�� PC �ӧҹ���ѹ
	if (idleRate) {
		if (idleCounter > 4) {
			idleCounter -= 5;
		} else {
            // ����ͤú�������ǡ�������
			idleCounter = idleRate;
			tx_kbd = 1;
			tx_mouse = 1;
		}
	}

    static uchar ms_counter = 0;
    static uint16_t led_counter = 0;
    // �ء�ͺ��÷ӧҹ�л���ҳ 170.666us ��ͧ�����ҳ 6 �ͺ����ҡѺ 1024us ���� 1.024ms
    // �ء� 100�Թҷ� ���Թ 2.4 �Թҷ� �ѧ��鹷ء� 50 �ͺ ��èе�ͧź 1ms (���ͧ��������Դ��Ҵ �Ҵ��ҹ�Ҩ��Դ�ҡ�� int)
    if (ms_counter++ >= 6)
    {
        ms_counter = 0;
        sys_millis++; // �����к������ 1ms
                
        // test on/off led
        if (led_counter++ >= 1000)
        {
            led_counter = 0;
            led_toggle();
        }
    }
}

// ����բ������ buffer �������������� ��� tx_kbd, tx_mouse �� True , ���觢������͡�
// ����͹ Wishabi project
void send_packets(void)
{
    // ���������ѹ�����á ��ѧ�ҡ 100ms ��鷴�ͺ�觢�����
    // �Ҵ��Ҫ���Ŵ�ѭ�� byte �á���(��� mouse ��� keyboard)
    static byte runOnce = false;
    if ( (!runOnce) && (sys_millis > 100) )
    {
        //inputBuffer1[1] = MOD_SHIFT_LEFT;
        //inputBuffer1[2] = KEY_A;
        tx_kbd = 1;
        tx_mouse = 1;
        runOnce = true;
    }

	/* send pending packets */
	if (usbInterruptIsReady()) {
		if (tx_kbd) {
			usbSetInterrupt(inputBuffer1, sizeof(inputBuffer1));	/* send kbd event */
			tx_kbd = 0;
            hid_keyboard_clear();
		} else if (tx_mouse) {
			usbSetInterrupt(inputBuffer2, sizeof(inputBuffer2));	/* send mouse event */
			tx_mouse = 0;
            hid_mouse_clear();
		}

        //led_off(); // �ʴ�ʶҹ��ѡ˹���
        // ������ҧ��� reenable usart rx interrupt 
        //UCSRB |= _BV(RXCIE);
	}
}

/* ---------------------- ����ǹ����ǡѺ project (����к�) ------------------------- */


/* ------------------------ Main Program --------------------------------------- */
int main(void)
{
	// ��鹰ҹ
	wdt_enable(WDTO_1S);
	odDebugInit();
    
	/* all inputs, pull-ups active */
	// ��駤�� port
	DDRB = _BV(5);	/* LED */
	DDRC = 0;
	DDRD = 0;
	PORTB = 0xff;
	PORTC = 0xff;
	PORTD = 0xff;

    // ��駤�� interrupt �ҡ��¹͡ (INT1 㹷�������Ѻ IR)
	//GICR |= _BV(INT1);	/* enable INT1 */
	//MCUCR = (MCUCR & ~(_BV(ISC11) | _BV(ISC10))) | _BV(ISC11);	/* set INT1 on falling edge */

    // ��駤�� timer 0
    //TCCR1B = 3;		/* prescaler = 1/64 -> 1 count = 5.33333 us */  ��Ǩ�Ѻ�ѭ�ҳ input �֧�����
	TCCR0 = 2;		    /* prescaler = 1/8 -> 0.6666us per loop ; overflow at 256 = 170.6666 us; 
                           //��ͧ�Ѻ����ҳ 6 �ͺ�֧��ҡѺ 1second = 1020us = 1.02ms */
    //TIMSK = (1<<TOIE0);    // ����¹�ҵ�駤��Ẻ interrupt

    // ��駤�� USART
    UART_init();

    // reset ��÷ӧҹ mode DirectMouse
    directMouseReset();
    
    // ��駤�� keyboard
    KBCC_setup();

	// reset ��� init
	usbReset();
	usbInit();
	sei();

	/* main event loop */
	for (;;) {		
		wdt_reset();
		usbPoll();			// ����觢ͧ library

        // 㹡óշ���ͧ�����ҹẺ�кص��˹� mouse ��觶������繧ҹ��觴�ǹ �зӧҹ����͹ loop ���
        if (directMouse.workingFlag)
        {
            processDirectMouse();
        }
        else
        {
            // ��Ǩ�����ҹ keyboard
            // ��Ǩ key, debounce, �� buffer, multiple key ���
            KBCC_loop();
        }

		/* on timer overflow (~22ms) 
		   �鹡�õ�Ǩ overflow Ẻ����� interrupt
		*/
		if( TIFR & _BV(TOV0) )
        {
            idle_timer();
            TIFR = _BV(TOV0);       // clear TOV0
		}

		// ����բ���������� �����͡�
		send_packets();

        // update �����¹ eeprom (�����)
        if (usart_updatingFlag)
            usartUpdateEEProm();
	}
	return 0;
}

/* ------------------------------------------------------------------------- */


// ���ѧ
// ***** ������� compile Ẻ -O1 ������ѹ �� hang �� reset
// �Ҵ��ҹ�Ҩ��Դ�ҡ -O1 ���Ŵ�ٻ��á��ⴴ ���Ẻ short �����������ǹ������㹾�鹷����¹�ҡ
// ��á��ⴴ �������ö��Ẻ  short �� ����Ҩ���ջѭ��
//
// bug
// x- ����֡��ҵ��˹觻����мԴ��Ҵ ���Ф�� 50 = END_BUTTON (��ͧ��¹��ǵ�Ǩ�Ѻ���� ����Ҵ���ҹ��)
//      **- �������Ѻ���ҷ���������� flash �֧���Ը�������˹���� -50 , 206
// x- ����֡��һ��� macro �зӧҹ��ҧ ���ӧҹ��ҧ �Ѻ DotA
//      - ����ԧ���Ƿӧҹ ���ѹ˹�ǧ�Թ 3000ms �ѹ����ѧ��������� �Ը�����������������ҡ��鹹Դ˹��� 3300ms
//      - ��һ��� macro �ӧҹ������ѹ �ѹ�зӧҹ���������§�ӴѺ �ͧ��á�����价��Чҹ
// x- ����ա����ѹ�ѧ˹�ǧ� �ӧҹ��� ��੾�е͹��������ҧ macro buffer 
//      - ��Ѻ���� �Դ��ҹ�Ҩ����ҷ�������������  
// x- �� EEPROM ���� mouse �ѧ�Ѻ�ͧ�ҧ����� ( �� keyboard ����� mouse ����) 
//      - ������
// x- ������Ըա�˹����˹� mouse ����� (�����������) 
//      - �����������
// x- ���ҷ�� delay �ѧ���ç (���䧴��� -_-)
//     **- ����Ѻ��Ҿ���ҷ�����ç (�������§) 
// x- alt (special) �ѧ��ҧ�š� 
//      - ������ ���¡����� cancel interrupt ����͡ loop �֧�ӧҹ������ �������顴���� (��͹˹�ҵ�ͧ�������֧¡��ԡ)
// x- ���� disable ���¡��ԡ�ء���� ���������Ѩ�غѹ
//      - ���º���� �¡�͹˹�ҷ���������еԴ����ͧ���� ����� add macro key ŧ仵ç�
// x- ���� macro ���ӧҹ��ҡ������ ����
//      - ���� macro �ѧ���ӧҹ ��з�Ẻ���§�ӴѺ�ѹ�
// 
// macro mouse : 
//    x �ѧ�Դ��Ҵ����ͧ�ش�������¨ش (���»Դ Enhance pointer precision)
// macro key   : 
//    x keyboard ������byte �á���
//    x interrupt �ѧ��ҹ��ԧ����� (�Ҩ������ǡѺ special key ����)

/***************************************************************************
 * Status          : Alpha 2/5/2552 15:27
 * Date start      : 20/4/2552 22:42
 * Project         : Macro key (Avatar Key Edition)
 * Project Code    : KBCC (����Ѻ�ء function ��� global variable)
 * Compiler        : AVR-GCC
 * MCU             : ATMEGA8 (V-USB project)
 * Clock           : 12Mhz
 * Programmer      : Ksemvis Vitoonrach
 * Email           : Ksemvis@gmail.com
 * Objective       : 
 *  - ����ö�� macrokeyboard ��
 *  - ���ͧ�� usb keyboard & mouse ��
 *  
 * Details
 *  - �繡�èѴ��� keyboard �� code �ҡ Project DotA Keyboard �ҵѴ function ��ҧ� ����ͧ�Ѻ��§ 4 ����
 *  - code �ҡ ultimate �� tab ��� 6 ����
 *  - �ҷ���� ��˹����Ţ�Ե �ͧ port ���� ������˹���������ŢẺ Arduino
 *  - �� port ��ҧ��ҧ 㹷������ portC ������ҧ����
 *  - ����觵�ҧ� �����������Ф����� 'extension'
 * News
 *  - ultimate keyboard version �������蹷�� port �ҡ arduino ����� GCC ��������������������ҹ�������СѺ AVR-GCC
 *  - ����ö�� macro key ���٧�ش 8 ���� ������ port �����
 *  - �纤��˹��¤������� 480 byte
 *
 *
 * [��÷��� macrokey]
 *  * - ��觷ӧҹ�� macrokey �� ���§����ӴѺ��
 *      - ͸Ժ���ٻẺ����红�����
 *        - ������ keyboard
 *          - �ٻẺ 200 �� keymap (keycode ����) ������¢����� 2 byte ���Ǩ����� 212 (mod + keycode + end)
 *          - �ٻẺ 201 �� mousemap 
 *          - �ٻẺ 207 �� ��á�˹� mouse Ẻ x,y ������÷ӧҹẺ directmode ������Դ�ٻ������ �����Ҩзӧҹ����
 *          - �ٻẺ 202 �� SpecailKey ���͡��Դ���Ѻ ������¢����� 2 byte ���Ǩ����� 212 (mod + keycode + end) **�� buffer �� keypad 7 ����
 *          - �ٻẺ 210 ����� block ����� Macrokey **�� buffer �� keypad 7 ����
 *              - ����� 200 �� keyboard 7 byte ��ҹ��¡��ҵ�ͧ������ 0 (��ҹ��)
 *              - ����� 201 �� mouse ������¤���ա 3 bytes 
 *              - ����� 203 �� Key disable ¡��ԡ���Ǥ��� ������ key������ ���ǡ�Ѻ��ʶҹ���� (�� 2 byte)
 *              - ����� 204 �� Key disable ¡��ԡ�����ҧ������
 *              - ����� 205 �� delay ������¢����� 2 byte ˹����� ms ���� 3 �� �� 3000
 *              - ����� 206 �� ����觨� �ͧ�ء����
 *              - ����� 207 �� ����觡�˹� ���˹� mouse
 *              - ����� 208 �� �������͹��Ѻ������������ �� 2 byte ��� cmd ��� parameter(���� 29/1/2553)
 *              - ����� 211 �� block ����� Macrokey �ͧ���Ъش
 *              - ����� 212 �� Macro �������繷������ش�ͧ���� key ���� (��Ң���һ���˹�� �� 212 �����һ��� 1 �� key ��ҧ ��������� assign ���)
 *        - ������ 201 �� mouse ������¤���ա 3 bytes ���Ǩ����� 212 
 *        - �ͧ�Ѻ��÷ӧҹ������ �ѹ�����ҧ mouse ��� keyboard  ���� 1 block ���դ�������٧�ش��§ 3 ����� ����ӡѹ
 *          ��� M-K-D ��ǹ
 *        ������ҧ 1 block :   210,201,m1,m2,m3,200,k1,..kn,211,210,205,3000,211     ....     212
 *        ������ҧ Ẻ�ѡ�� :    [   M   -  -  -  K   - .. -  ]    [  D    -   ]      ....      End
 *        � block macro ���� Keyboard �Ѻ mouse �����ҧ�е����ҹ�� �������ҧ����ҧ˹����ҹ��
 *
 *        ������ҧ special key : 202,m0,m1,212
 *        ������ҧ Ẻ�ѡ�� :      [   -  -  ]
 *
 * *** �����˵� : 
 *    - ��÷ӧҹẺ macrokey ��� ���������͹ Fix key �����ѹ���ѧ���ӧҹ�����Ҩ�����顴 key ����
 *    ����͡��˹�觤��駷���� macrokey ��һ����ж١������� buffer �ա�ѹ˹�� ��ҡ��ա���駨��繡������͡
 *    - �����ҧ��� macrokey ���ѧ�ӧҹ (�ѧ�������ش macro) �����ա����������� ������ö�ӧҹ��ҹ�ѹ��� �����Դ����� 
 *    ����ͧ�ѹ ������¡��ԡ�������� �����ѧ�ӧҹ����
 *    - ����� �� macrokey ��è��� led ��о�Ժ���� ���͵Դ��ʹ���� �����駼���� (Option)
 *    - ��������º���� � LED �С�о�Ժ�ء� 1 sec �»���ҳ (�����һ���)
 *    - ��� update eeprom led �еԴ��ҧ��ʹ
 *
**/

/*************************** Constant Define *******************************/ 
// extension for GCC
#define byte        uchar               
#define true        1
#define false       0

#define KBCC_DEBUG  false

// keyboard and mouse config
#define KEYBOARD_BUFFER_SIZE    7
#define MOUSE_BUFFER_SIZE       3

// mouse speed
#define MOUSE_SPEED_LEFT    -10
#define MOUSE_SPEED_RIGHT   10
#define MOUSE_SPEED_UP      -10
#define MOUSE_SPEED_DOWN    10

// ��駤�� ˹�ǧ���� debounce
#define MAIN_DEBOUNCE               20
#define SPECIAL_KEY_DEBOUNCE        250
#define MACRO_KEY_DEBOUNCE          SPECIAL_KEY_DEBOUNCE

// ���˹�ǧ���ҷ��������ѭ�ҳ repeat �蹡�á���ҧ �������Թ������ �ռŵ��ʶҹТͧ��� "��������ҧ" �������ǡѺ debounce
#define WAIT_MOUSE_REPEAT           4  // �ӧҹ�� 2-4(����ʶ���)-7 ����� UART 2
#define WAIT_KEYBOARD_REPEAT        15  // 15 ����� UART 2

// ���˹�ǧ���ҷ��������ѭ�ҳ release �ѹ�繡�� reset key �óշ��¡�����ҹ���� (��ͧ�ѹ��ä�ҧ)
#define WAIT_MOUSE_RELEASED         12  // �ӧҹ�� 5-8(����ʶ���)-14 ����� UART 10
#define WAIT_KEYBOARD_RELEASED      22  // 22 ����� UART 10

// ���˹�ǧ���� ���������ش macro block ��Ѻ keyboard ��� mouse ������ǡ��ҹ��мԴ��Ҵ
// ** ��ͧ���
// �����Ѻ keyboard ��è��� 40
// �����Ѻ mouse ��è��� 20 ���͹��¡���
#define WAIT_END_MACRO_BLOCK            5    // 5 ����ʶ���
#define WAIT_KEYBOARD_END_MACRO_BLOCK   25   // 25 �ӧҹ�� (��ͧ���ͺ�ա�ѡ�ѡ) 35 �ʶ���
#define WAIT_MOUSE_END_MACRO_BLOCK      15   // 15 �ӧҹ�� (��ͧ���ͺ�ա�ѡ�ѡ) 35 �ʶ���

// ��������´��� WAIT_MOUSE_END_MACRO_BLOCK �ӧҹ�� (WORK !!)
// MRR = MAIN_DEBOUNCE/WAIT_REPEAT/WAIT_RELEASE
// 28 ��� MRR=20/15/22  (25 ��� work �������ҡ��ǵԴ��͡ѹ, 28 work )
// 11 ��� MRR=20/4/8 �����������¹����觺ҧ���ҧ������͹ mouse ������ѹ��� x,y ��Ѻ��� work �֧��Ѻ��� 28 ������
// ��� work 9
// 10 ???

// �ش����觷�������� Macro Key
#define MACRO_TYPE_KEYBOARD         200
#define MACRO_TYPE_MOUSE            201
#define MACRO_SPECIAL_KEY           202     // ���Դ/���Ѻ
#define MACRO_TYPE_MOUSE_XY         207     // ����觡�˹����˹� mouse x,y

#define MACRO_BLOCK_START           210     // ������ش����� macro
#define MACRO_BLOCK_STOP            211     // ���ش����� macro ���������������ش����觵���
//#define MACRO_END                   212     // ����ش����觷�����  ¡���ԡ�����ҹ ������ END_BUTTON ᷹��

#define MACRO_CMD_KEYBOARD          MACRO_TYPE_KEYBOARD
#define MACRO_CMD_MOUSE             MACRO_TYPE_MOUSE
#define MACRO_CMD_MOUSE_XY          MACRO_TYPE_MOUSE_XY
#define MACRO_CMD_INTERRUPT         203     // ¡��ԡ�������ɷ����� ���Ǥ���
#define MACRO_CMD_DISABLE_ALL       204     // ¡��ԡ�������ɷ����� ����
#define MACRO_CMD_DELAY             205     // ��� delay
#define MACRO_CMD_REPEAT            208     // ����������͹��Ѻ价ӧҹ (29/1/2553)

#define END_BUTTON                  206     // ������觢ͧ�ء�����, ���繵�ǹѺ�������л���

#define MACRO_NULL                  255     // ��͡�˹��繤�� Free ��������ش�����ء����

/*************************** Global Define *********************************/
unsigned long mouse_lasttime = 0;           // �纤������˹�ǧ���� mouse ��ͧ�ѹ����觢����������Թ�
unsigned long keyboard_lasttime = 0;        // �纤������˹�ǧ���� keyboard ��ͧ�ѹ����觢����������Թ�
unsigned long specialKey_lasttime = 0;      // �纤������˹�ǧ���� �������Դ���Ѻ Special ��ͧ�ѹ����觢����������Թ�
unsigned long macroKey_lasttime = 0;        // �纤������˹�ǧ���� �������Դ���Ѻ macro ��ͧ�ѹ����觢����������Թ�
unsigned long macro_global_time = 0;        // �纤�����ҷ���ҹ� ����������ż� macro

byte waitReleaseKeyboard = false;           // ��Ǩ��ҵ�ͧ��Ѻ�� release key ������� (reset)
byte waitReleaseMouse = false;              // �� ���ͷ��е�ͧ��Ѻ�� release

//
// �ç���ҧ ��е���� �����ҹ��ҹ Macro
//
struct Keyboard_buffer_t
{
    byte        buffer[KEYBOARD_BUFFER_SIZE];
    byte        index;
};
struct SpecialKey_buffer_t
{
    byte        buttonNo;       // ���Ţ keypad
    uint16_t    address;        // address �ͧ����
};
struct macro_buffer_t
{
    byte        buttonNo;       // ���Ţ keypad
    uint16_t    address;        // ���Ţ address eeprom
    uint16_t    delay;          // ����ա�� delay
};

// �纤�ҵ����Ẻ�Ẻ��ҧ� ����բ�Ҵ���������ҡѺ KEYBOARD_BUFFER_SIZE
struct Keyboard_buffer_t    keyboard_buffer;            // �� keyboard �����§���� �������
// �纤�ҵ����Ẻ�Ẻ��ҧ� ����բ�Ҵ���������ҡѺ MOUSE_BUFFER_SIZE
byte   mouse_buffer[MOUSE_BUFFER_SIZE];                 // �� Mouse �����§���� �������

// �� keycode ���Դ����´Ѻ
struct SpecialKey_buffer_t  special_buffer[KEYBOARD_BUFFER_SIZE]; // ����һ��� special � �١��仺�ҧ
byte   specialKeyIndex = 0;                             // ����ҵ͹��� speial key ��仡���������
// ���纻������ 7 �ͧ macrokey (��駻��� macro �����ӡѴ ���������ѹ���� 7)
struct macro_buffer_t macro_buffer[KEYBOARD_BUFFER_SIZE];   // macro key �٧�ش 7 ���� ������ѹ 
byte   macroKeyIndex = 0;                                   // ����ҵ͹��� macro key ��仡���������
byte   macro_interrupt_flag = false;                        // ����� true �ʴ���� interrupt ������觢����Ż���

// (option)������� addres �ͧ eeprom ��Ѵ�ӴѺ������ҧ� ����������ҳ�˹��ǧ�˹
// ��������������觢�� ����Ѻ initialAddrEEprom 㹡�ä�����ШѴ�ӴѺ�����á
// uint16_t eepromAddrButton[3];
// ���Ҥ��ҡ����� ��������ͧ��������ǧ�˹ �� initial �ҡ eepromAddrButton ���������
//


// ����÷���������Ѻ main program
extern unsigned long sys_millis;                        // ����¡��ҡ�к� // extension
extern uchar tx_kbd;                                    // ���Ǩ��͹��
extern uchar tx_mouse;                                  // ���Ǩ��͹��
extern uchar inputBuffer1[KEYBOARD_BUFFER_SIZE+1];      // keybuff �������Ѻ main program +1 ���з�� main byte �á�к��繻����� 1 �� keyboard
extern uchar inputBuffer2[MOUSE_BUFFER_SIZE+1];         // mousebuff �������Ѻ main program +1 ���з�� main byte �á�к��繻����� 2 �� mouse
extern struct DirectMouse directMouse;                  // ��Ѻ����� ��˹����˹� XY mouse
//extern uchar IsNumlockOn;                               // �纤�һ��� numlock ��� on ���� off

/*************************** Global Function *******************************/
extern void usartPutchar(uchar ch);
extern void usartSendWord(int16_t wd);

/*************************** Special Block Key Code ************************/

/* Keyboard usage values, see usb.org's HID-usage-tables document, chapter
 * 10 Keyboard/Keypad Page for more codes.
 * ���ʹ�� Appendic C scancode.doc �ͧ Microsoft http://www.microsoft.com/whdc/archive/scancode.mspx#ENB
 */

// mouse
#define LEFT_CLICK          (1<<0)
#define RIGHT_CLICK         (1<<1)

// keyboard
#define MOD_CONTROL_LEFT    (1<<0)
#define MOD_SHIFT_LEFT      (1<<1)
#define MOD_ALT_LEFT        (1<<2)
#define MOD_GUI_LEFT        (1<<3)
#define MOD_CONTROL_RIGHT   (1<<4)
#define MOD_SHIFT_RIGHT     (1<<5)
#define MOD_ALT_RIGHT       (1<<6)
#define MOD_GUI_RIGHT       (1<<7)

#define KEY_NONE         0
#define KEY_ROLL_ERROR   1
#define KEY_POST_FAIL    2
#define KEY_UNDEFINED_ERROR 3

#define KEY_A            4
#define KEY_B            5
#define KEY_C            6
#define KEY_D            7
#define KEY_E            8
#define KEY_F            9
#define KEY_G           10
#define KEY_H           11
#define KEY_I           12
#define KEY_J           13
#define KEY_K           14
#define KEY_L           15
#define KEY_M           16
#define KEY_N           17
#define KEY_O           18
#define KEY_P           19
#define KEY_Q           20
#define KEY_R           21
#define KEY_S           22
#define KEY_T           23
#define KEY_U           24
#define KEY_V           25
#define KEY_W           26
#define KEY_X           27
#define KEY_Y           28
#define KEY_Z           29

#define KEY_1           30
#define KEY_2           31
#define KEY_3           32
#define KEY_4           33
#define KEY_5           34
#define KEY_6           35
#define KEY_7           36
#define KEY_8           37
#define KEY_9           38
#define KEY_0           39

#define KEY_ENTER       40
#define KEY_ESCAPE      41
#define KEY_BACKSPACE   42
#define KEY_TAB         43
#define KEY_SPACE       44
#define KEY_DASH        45
#define KEY_EQUALS      46
#define KEY_LPAREN      47
#define KEY_RPAREN      48
#define KEY_BACKSLASH   49

#define KEY_NON_US_HASH 50
#define KEY_SEMICOLON   51
#define KEY_QUOTE       52
#define KEY_TILDE       53
#define KEY_COMMA       54
#define KEY_DOT         55
#define KEY_SLASH       56

#define KEY_CAPS_LOCK   57

#define KEY_F1          58
#define KEY_F2          59
#define KEY_F3          60
#define KEY_F4          61
#define KEY_F5          62
#define KEY_F6          63
#define KEY_F7          64
#define KEY_F8          65
#define KEY_F9          66
#define KEY_F10         67
#define KEY_F11         68
#define KEY_F12         69

#define KEY_PRINT_SCREEN 70
#define KEY_SCROLL_LOCK 71
#define KEY_PAUSE       72
#define KEY_INSERT      73
#define KEY_HOME        74
#define KEY_PAGE_UP     75
#define KEY_DELETE      76
#define KEY_END         77
#define KEY_PAGE_DOWN   78

#define KEY_RIGHT_ARROW 79
#define KEY_LEFT_ARROW  80
#define KEY_DOWN_ARROW  81
#define KEY_UP_ARROW    82

#define KEY_NUM_LOCK    83

#define KEY_NUM_DIV     84
#define KEY_NUM_MUL     85
#define KEY_NUM_SUB     86
#define KEY_NUM_ADD     87

#define KEY_NUM_ENTER   88
#define KEY_NUM_1       89
#define KEY_NUM_2       90
#define KEY_NUM_3       91
#define KEY_NUM_4       92
#define KEY_NUM_5       93
#define KEY_NUM_6       94
#define KEY_NUM_7       95
#define KEY_NUM_8       96
#define KEY_NUM_9       97
#define KEY_NUM_0       98
#define KEY_NUM_DOT     99

/***********************************************************************************************/


// ������ҧ macro buffer ��͹�����ҹ��ԧ
// uchar  sampleMacroBuffer[MAX_MACROKEY_SIZE] = {
        /* // test keyboard
        MACRO_TYPE_KEYBOARD,0,KEY_UP_ARROW,END_BUTTON,    // ���� 1
        MACRO_TYPE_KEYBOARD,0,KEY_DOWN_ARROW,END_BUTTON,  // ���� 2
        MACRO_TYPE_KEYBOARD,0,KEY_LEFT_ARROW,END_BUTTON,  // ���� 3
        MACRO_TYPE_KEYBOARD,0,KEY_RIGHT_ARROW,END_BUTTON, // ���� 4
        MACRO_TYPE_KEYBOARD,MOD_SHIFT_LEFT,0,END_BUTTON,  // ���� 5
        MACRO_TYPE_KEYBOARD,0,KEY_LPAREN,END_BUTTON,      // ���� 6
        MACRO_TYPE_KEYBOARD,0,KEY_RPAREN,END_BUTTON,      // ���� 7
        MACRO_TYPE_KEYBOARD,MOD_CONTROL_LEFT | MOD_ALT_LEFT,KEY_DELETE,END_BUTTON, // ���� 8
        */
        /* // test mouse and some keyboard
        MACRO_TYPE_MOUSE,LEFT_CLICK,0,0,END_BUTTON,           // ���� 1
        MACRO_TYPE_MOUSE,0,MOUSE_SPEED_RIGHT,0,END_BUTTON,    // ���� 2
        MACRO_TYPE_MOUSE,0,0,MOUSE_SPEED_DOWN,END_BUTTON,     // ���� 3
        MACRO_TYPE_MOUSE,0,MOUSE_SPEED_LEFT,0,END_BUTTON,     // ���� 4
        MACRO_TYPE_MOUSE,RIGHT_CLICK,0,0,END_BUTTON,          // ���� 5
        END_BUTTON,                                           // ���� 6 NONE
        MACRO_TYPE_MOUSE,0,0,MOUSE_SPEED_UP,END_BUTTON,       // ���� 7
        MACRO_TYPE_KEYBOARD,MOD_SHIFT_LEFT,0,END_BUTTON,      // ���� 8
        */
        /* // test Special Key, key , mouse
        MACRO_SPECIAL_KEY,0,KEY_RPAREN,END_BUTTON,            // ���� 1
        MACRO_SPECIAL_KEY,0,KEY_LPAREN,END_BUTTON,            // ���� 2
        MACRO_SPECIAL_KEY,0,KEY_INSERT,END_BUTTON,            // ���� 3
        MACRO_SPECIAL_KEY,0,KEY_DELETE,END_BUTTON,            // ���� 4
        MACRO_TYPE_KEYBOARD,0,KEY_RIGHT_ARROW,END_BUTTON,     // ���� 5
        MACRO_TYPE_KEYBOARD,0,KEY_LEFT_ARROW,END_BUTTON,      // ���� 6
        MACRO_TYPE_MOUSE,0,0,MOUSE_SPEED_DOWN,END_BUTTON,     // ���� 7
        MACRO_TYPE_MOUSE,0,MOUSE_SPEED_LEFT,0,END_BUTTON,     // ���� 8
        */
        /* // test Macro mouse and somekeyboard !!!
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,2,0, MACRO_BLOCK_STOP,         // move right
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,0,2, MACRO_BLOCK_STOP,      // move down
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,-2,0, MACRO_BLOCK_STOP,      // move left            
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,2,0, MACRO_BLOCK_STOP,         // move right
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
        END_BUTTON,
        */
        /*
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,50,0, MACRO_BLOCK_STOP,         // move right
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,0,50, MACRO_BLOCK_STOP,      // move down
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,-50,0, MACRO_BLOCK_STOP,      // move left            
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            // �ͺ 2
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,50,0, MACRO_BLOCK_STOP,         // move right
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,0,50, MACRO_BLOCK_STOP,      // move down
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,-50,0, MACRO_BLOCK_STOP,      // move left            
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,50,0, MACRO_BLOCK_STOP,         // move right
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,  // click
            END_BUTTON,
        */
        
        // test Macro keyboard 
        // ��������� 1
        /*
        MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_A,0, MACRO_BLOCK_STOP,                    // a
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_A,0, MACRO_BLOCK_STOP,  // A
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_B,0, MACRO_BLOCK_STOP,                // b
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_B,0, MACRO_BLOCK_STOP,  // B
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_C,0, MACRO_BLOCK_STOP,                
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_C,0, MACRO_BLOCK_STOP,  
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_D,0, MACRO_BLOCK_STOP,                
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_D,0, MACRO_BLOCK_STOP,  
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_E,0, MACRO_BLOCK_STOP,                
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_E,0, MACRO_BLOCK_STOP,  
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F,0, MACRO_BLOCK_STOP,                
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_SHIFT_LEFT, KEY_F,0, MACRO_BLOCK_STOP, 
            END_BUTTON,
        */
        // ���� 2
        /*
        MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, MOD_CONTROL_LEFT | MOD_ALT_LEFT, KEY_DELETE,0, MACRO_BLOCK_STOP,
        END_BUTTON,
        */        
        /*
        // ���ͧ delay , interrupt ��� disable key
        // ���� 1 Special ALT key
        MACRO_SPECIAL_KEY,MOD_ALT_LEFT,0,                                                   // alt
        END_BUTTON,                
        // ���� 2 ¡��ԡ���� 'a'
        MACRO_BLOCK_START, MACRO_CMD_INTERRUPT, 0,KEY_A, MACRO_BLOCK_STOP,                  // i-a
        END_BUTTON,         
        // ���� 3 ¡��ԡ���� 's' for stop -> ¡��ԡ���� 'a' for attack
        MACRO_BLOCK_START, MACRO_CMD_INTERRUPT, 0,KEY_S, MACRO_BLOCK_STOP,                  // i-s
            MACRO_BLOCK_START, MACRO_CMD_INTERRUPT, 0,KEY_A, MACRO_BLOCK_STOP,              // i-a
        END_BUTTON,     
        // ���� 4 ����͹ mouse ���� delay 3 �� ��������͹ mouse ���� delay 3 �� ��������͹ mouse �ա����
        MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,50,0, MACRO_BLOCK_STOP,                      // move right
            MACRO_BLOCK_START, MACRO_CMD_DELAY, 0x07 , 0xD0, MACRO_BLOCK_STOP,              // delay 2000ms
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,0,50, MACRO_BLOCK_STOP,                  // move down
            MACRO_BLOCK_START, MACRO_CMD_DELAY, 0x07 , 0xD0, MACRO_BLOCK_STOP,              // delay 2000ms
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,50,0, MACRO_BLOCK_STOP,                  // move right
            MACRO_BLOCK_START, MACRO_CMD_DELAY, 0x07 , 0xD0, MACRO_BLOCK_STOP,              // delay 2000ms
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, 0,0,50, MACRO_BLOCK_STOP,                  // move down
        END_BUTTON,
        // ���� 5 disable all
        MACRO_BLOCK_START, MACRO_CMD_DISABLE_ALL, MACRO_BLOCK_STOP,                         // disable all
        END_BUTTON, 
        */
        /*
        // �������ǡ�Ѻ��ҹ
        // 0-0-f1-num5-click  
        MACRO_BLOCK_START,     MACRO_TYPE_KEYBOARD, 0,KEY_0,0, MACRO_BLOCK_STOP,                // 0
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_0,0, MACRO_BLOCK_STOP,                // 0
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F1,0, MACRO_BLOCK_STOP,               // F1
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_NUM_5,0, MACRO_BLOCK_STOP,            // num5
            MACRO_BLOCK_START, MACRO_TYPE_MOUSE, LEFT_CLICK,0,0, MACRO_BLOCK_STOP,              // click
            // option
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F1,0, MACRO_BLOCK_STOP,               // F1
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F1,0, MACRO_BLOCK_STOP,               // F1            
            MACRO_BLOCK_START, MACRO_CMD_DELAY    , 0x0b, 0xb8, MACRO_BLOCK_STOP,               // DELAY 3sec
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F1,0, MACRO_BLOCK_STOP,               // F1
            MACRO_BLOCK_START, MACRO_TYPE_KEYBOARD, 0,KEY_F1,0, MACRO_BLOCK_STOP,               // F1
        END_BUTTON,

        END_BUTTON,
        MACRO_NULL,  // << �騺�ء���� 㹡óշ����������ͷ��
    }; */
                        
                        //*************************************************************************/
                        //
                        // �������ǹ keyboard
                        //
                        /***************************************************************************
                         * Status          : Complete !!
                         * Date            : 9/4/2552 8:34
                         * Project         : Ultimate KeyMatrix (debounce without delay and multiple keypress support !!)
                         * Project Code    : KMAT (����Ѻ�ء function ��� global variable)
                         * Compiler        : Arduino - 0015
                         * MCU             : ATMEGA168
                         *                 : ATMEGA8 (Arduino SingleBoard ���͡ Arduino NG or older w/ ATmega8)
                         * Clock           : 16Mhz
                         * Programmer      : Ksemvis Vitoonrach
                         * Email           : Ksemvis@gmail.com
                         * Objective       : 
                         *    - keymatrix Ẻ����͡��˹�ǧ���� (delay) ���� polling ���кҧ�ҹ�������ö delay ��
                         *    - �� internal r-pullup ����ͧ�����¹͡
                         *    - �������Ҩ���顴��ҧ�� ���ͨ���顴���������
                         *    - �顴Ẻ�������� �������¤��������ѹ�� (������ӴѺ��á���͹��ѧ)
                         *    - �Ѳ�������������� portable
                         *
                         * Update          : 10/4/2552 22:46
                         *    - �� bug ������ 1 �Ѻ 24 �ժ�ǧ���������ҡѹ (���Ǩ��͹��Ҥú 1ms �����ѧ ��Ҥú���Ǥ��ºǡ ���� MCU �ӧҹ���Թ�)
                         *    - ��Ѻ��ا���͹䢡��ǹ�ٺ���բ�� ��ǡ loop ����÷Ѵ�ش���� �����������ش�ҡ loop ������ͧ�����������ա����
                         *    - �ͧ�Ѻ��á����»���������ѹ 㹤��� Rows �� �� columns ���ǡѹ����� (�ѭ��ǧ�� �����Ẻ matrix �������)
                         *    - ��������� 15/4/2552 (���¡�����ͧ�����ѹ)
                         *
                         *
                         * ��觷���ͧ�Ѳ������
                         *    x ���ͧ�� Slave I2C �� ��������ö��觨ҡ Master MCU 
                         *      -> ¡��ԡ ���ͧ�ҡ�������ѹ������� ��ǹ����ͧ I2C ���¡���¹���ա���˹���� i2c Master & Slave implement
                         *         ������ҹ �����Ը� copy source (implement) ������� ��������Ѻʹ
                         *
                         * �й�
                         *  - ���ҷ��ͧ����Դ� hyperterminal ����Դ� Arduino Serialport ������͹��Ҩ� �觢����żԴ��Ҵ����
                         *    ���Դ� hyperterminal �Դ�� 2 ��. ����� error
                         *
                         *
                         * BUG
                         *    
                         *
                         * [�Ըա����ҹ] (step by step)
                         * 1. copy ������ 仨�����ش���͹���� (�����ǹ setup() ��� loop() ���)
                         * 2. ����¹��� KMAT_ALL_ROWS, KMAT_ALL_COLS �����ͧ���
                         * 3. ��˹���Ң����ç�������¡�� KMAT_setup()
                         *
                         * [�����˵ء����ҹ]
                         *  - ���Ҥ���� "(���)"    ���¶֧ �����䢤�����ç�Ѻ�����繨�ԧ
                         *  - ���Ҥ���� "(��䢡���)" ���¶֧ ��ҷ������ö ��� ���ͨл���¼�ҹ����
                         *
                         * [��ͨӡѴ]
                         * - �ͧ array ��Ẻ��µ������Ѻ columns ��� rows �����ҡ�����蹵�ͧ�� constant
                         *
                         * [�ٻẺ��õ�����ǧ��]
                         *
                         *  Row N ----[B N]----------[B N]----------[B N]----------[B N]----
                         *              |              |              |              |
                         *              |              |              |              | 
                         *  Row 1 ----[B 5]----------[B 6]----------[B 7]----------[B 8]----
                         *              |              |              |              |
                         *              |              |              |              |
                         *  Row 0 ----[B 1]----------[B 2]----------[B 3]----------[B 4]----
                         *   ^          |              |              |              |
                         *   |          |              |              |              |
                         *  (0) - >   Col 0          Col 1          Col 2          Col N
                         *
                         * ͸Ժ�� : 
                         * - Row0 ��� Col0 ����������������� B ��ͤ�ҷ��� return ��Ѻ�� byte
                         * - ��� index �ͧ array �����Ѻ���� debounce ����ö�ӹǹ��ҡ�ٵ� 
                         *        (RowX * Column_size) + ColumnX
                         * - ��� return ����ö�ӹǹ��ҡ�ٵ� 
                         *        (RowX * Column_size) + ColumnX + 1 
                         * - �������ա�á� key �Ф׹��� 0
                         * - �� Rows ���� output , �� Cols ���� input
                         */

                        /*************************** Define and Constant ***************************/ 
                        // ��˹� port,pin ������ ��Ф�Ҿ�鹰ҹ��� Arduino ��
                        #define PORT                PORTC
                        #define PIN                 PINC
                        #define DDR                 DDRC
                        #define OUTPUT              1
                        #define INPUT               0
                        #define HIGH                1
                        #define LOW                 0

                        #define KMAT_DEBUG          false                           // �Դ(true)/�Դ(false) �к� debug (���)
                                                                                    // ��� true ���� output �ҷ�� UART 19200
                        #define KMAT_ALL_ROWS       2                               // �ӹǹ �Ƿ����� (���)
                        #define KMAT_ALL_COLS       4                               // �ӹǹ ������������ (���)
                        #define KMAT_ALL_KEYS       KMAT_ALL_ROWS * KMAT_ALL_COLS   // �ӹǹ���������
                        #define KMAT_KEYBUFFER_MAX  4                               // �� key ������ѹ���٧�ش 4 ���� ��ŧ buffer (���)

                        /*************************** Global Define *********************************/
                        byte KMAT_leg_rows[KMAT_ALL_ROWS];       // �红ҷ��������
                        byte KMAT_leg_cols[KMAT_ALL_COLS];       // �红ҷ������ columns
                        byte KMAT_timer_counter[KMAT_ALL_KEYS];  // ��ǹѺ���ҷ���ҹ���ѧ��á� key (0=����顴, 1-250=���� debounce, >250=ʧǹ���ʶҹ����
                        byte KMAT_debounce_ms;                   // ���ҷ������Ǩ��� debounce (milliseconds) �դ���� 1-250
                        unsigned long KMAT_global_ms;            // �纤�����ҷ���ҹ仢ͧ�к�
                        byte KMAT_keypadBuffer[KMAT_KEYBUFFER_MAX]; // keyboard buffer
                        byte KMAT_keypadBuffer_index;               // �͡��ҵ͹����� key �˹��¤����ӡ��������

                        // ����ͷӧҹ�ա���駨�������Ѻ�������� ����ѭ�������������������� return �ѹ�� �з���顴��������� �����»���������ѹ�����
                        // ������蹹��������Ҩ���ҹ��ҷ���� pin �������ҹ���� port 
                        // ����� Global �ѹ���������Ǩ���ҶѴ���ͺ���� ����ͤú�ء rows+cols ���Ǩ���������� ����͹��ҡ����»���������ѹ��
                        byte KMAT_currentRows = 0;                // ��� KMAT_readkey();
                        byte KMAT_currentCols = 0;                // ��� KMAT_readkey();

                        /*************************** Global Function *******************************/

                        // extension function ����� ���������� GCC ����� Arduino 
                        // test ���Ǽ�ҹ !!
                        void pinMode(char pin,char mode)   // pass
                        {
                          if (mode)
                            DDR |= (1<<pin);
                          else
                            DDR &= ~(1<<pin);
                        }
                        void digitalWrite(char pin,char mode) // pass
                        {
                          if (mode)
                            PORT |= (1<<pin);
                          else
                            PORT &= ~(1<<pin);
                        }
                        char digitalRead(char pin) // pass
                        {
                           return (PIN&(1<<pin)) >> pin;
                        }
                        unsigned long millis()
                        {
                            return sys_millis;
                        }

                        // ��ҹ��� key ���л��� ����ա�á��� return, ����ա� 0
                        // ��� pressOnce = true  ���¶֧ ����������� �еԴ�ա��������ͻ����������ǡ�����
                        // ��� pressOnce = false ���¶֧ ���顴��ҧ�� ʶҹШ��ѧ�����͡�ҵ�����ͧ
                        byte KMAT_readkey(byte pressOnce)
                        {                        
                          // ��Ǩ�ͺ��Ҥ�� KMAT_currentRows �ú�ӹǹ �ҷ���������ѧ ���ǹ�ͺ�ú���� �����������
                          if (KMAT_currentRows >= KMAT_ALL_ROWS)
                              KMAT_currentRows = 0;
                          // ����������ҹ��� ǹ�ٻ�觤�� 0      
                          for ( ;KMAT_currentRows < KMAT_ALL_ROWS;  ) // �ǡ rows ������ҧ�ش�ͧ loop
                          {
                            digitalWrite(KMAT_leg_rows[KMAT_currentRows], LOW);

                            // ��Ǩ�ͺ��Ҥ�� KMAT_currentCols �ú�ӹǹ �ҷ���������ѧ ���ǹ�ͺ�ú���� ����������� (����͹ KMAT_currentRows)
                            if (KMAT_currentCols >= KMAT_ALL_COLS)
                                KMAT_currentCols = 0;
                            // �������ҹ��ҡ�á��ҡ��� Cols
                            for ( ;KMAT_currentCols < KMAT_ALL_COLS;  ) // �ǡ cols ������ҧ�ش�ͧ loop
                            {
                              // ���˹� array index �������Ǩ�ͺ�纤�� debounce �ҡ��á�����
                              byte index = (KMAT_currentRows * KMAT_ALL_COLS) + KMAT_currentCols;
                              // ��Ǩ������ա�á�����
                              if (digitalRead(KMAT_leg_cols[KMAT_currentCols]) == LOW)
                              {
                                //
                                // �Ѵ�����ǹ debounce
                                //
                                // ��Ǩ��ҡ�͹˹�ҹ���ա��������Ѻ���������(����������) ����繡ó� pressOnce 
                                // ������ա�� debounce �ҡ�͹˹�ҹ�騹�ú�������� ���ѧ������¡���
                                if (KMAT_timer_counter[index] >= KMAT_debounce_ms && pressOnce)
                                {
                                  // ����ͧ������ �����¡��ͨ����������ͧ
                                }
                                // ������ա�á��������� ���ѧ���ú���� �֧��Ǩ��ҡ�͹˹�ҹ���ա��������Ѻ���������ѧ Ẻ����
                                else if (KMAT_timer_counter[index] > 0)
                                {
                                  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                  // ��͹�������к� ��ͧ��Ǩ millis() ����Թ 49 �ѹ 9 ��.������� ���Сó��Թ���� �ѹ������� 0 ���� (��Ҩй��¡��� KMAT_global_ms)
                                  if (KMAT_global_ms > millis())
                                      KMAT_global_ms = millis(); // ��Ѻ��������        
                                  // ���˵ط��������ç��� ������Ҷ�����͡�ٻ�ѹ�е�Ǩ��ͺ��ʹ����
                                  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                  //
                                  // ������ҷ���ҹ� �Һǡ , ���������Ѻ����� ��Ǩ��������ҡ��� debounce �����ѧ ��������һѨ�غѹ + �Ѻ���ҷ�衴 = ���ҷ���ҹ���
                                  if (millis() - KMAT_global_ms > 1)
                                  {
                                    KMAT_timer_counter[index] += (byte)(millis() - KMAT_global_ms);
                                    // �ѹ�֡��������ش���ӧҹ
                                    KMAT_global_ms = millis();
                                  }
                                  // ��Ǩ���Ҥú debounce ���� ������ҡ���ԧ ����� return ���
                                  if (KMAT_timer_counter[index] >= KMAT_debounce_ms)
                                  {
                                    // clear ������� �ó� pressOnce = false �С���ҧ�� ��÷������ 0 ������ͺ˹�Ҩ����loop ��� �֧����繡�á���ҧ
                                    // (*** �ҡ����������� ����ö���仵�Ǩ��÷���ѧ���¡��͢����)
                                    if (!pressOnce)
                                        KMAT_timer_counter[index] = 0;
                                    // �׹��� port ���蹹�鹨� ����ʹ���
                                    digitalWrite(KMAT_leg_rows[KMAT_currentRows], HIGH); 
                                    // ���� cols �ա 1 (������ͧ������ç for..loop) ���蹹�� �����ش�͡仨С�Ѻ�ҫ���ͺ����ա˹��͹�ǡ  
                                    KMAT_currentCols++;
                                    return index + 1;
                                  }
                                }
                                else // �ѧ�����������Ѻ ��������������Ѻ
                                {
                                  KMAT_timer_counter[index] = 1;
                                  // �ѹ�֡��������ش���ӧҹ
                                  KMAT_global_ms = millis();
                                }        
                              }
                              else // �ç����Ǩ���� ������á����� (�������繡�� unpress)
                              {
                                KMAT_timer_counter[index] = 0;
                              }
                              // ���� cols �ա 1 (������ͧ������ç for..loop) ���蹹�� �����ش�͡仨С�Ѻ�ҫ���ͺ����ա˹��͹�ǡ  
                              KMAT_currentCols++;
                            }
                            // �ú�ͺ Row 1 �� �����׹��� port Row ���蹹�鹨� ������ "��" ��ʹ���
                            digitalWrite(KMAT_leg_rows[KMAT_currentRows], HIGH); 
                            // ���� rows �ա�� (������ͧ������ç for..loop) ���蹹�� �����ش�͡仨С�Ѻ�ҫ���ͺ����ա˹��͹�ǡ
                            KMAT_currentRows++;
                          }
                          
                          // �ӧҹ�����稤ú�ء�ͺ �ͺ������������ա�á������� return 0 
                          return 0;
                        }

                        // ź���� keybuffer
                        void KMAT_clear_keybuffer()
                        {
                          byte count = 0;
                          for(; count < KMAT_KEYBUFFER_MAX; count++)
                            KMAT_keypadBuffer[count] = 0;
                            
                          // �������Ǫ�� index �ͧ keypad buffer
                          KMAT_keypadBuffer_index = 0;
                        }

                        // ź������ keybuffer array ��� index ����к� ��������͹�ӴѺ���仢����
                        void KMAT_remove_keybuffer(byte index)
                        {
                          byte count = index;
                          for (; count < KMAT_keypadBuffer_index; count++)
                            KMAT_keypadBuffer[count] = KMAT_keypadBuffer[count+1];
                            
                          // �������Ѻ������ش���� �������ش������ 0  ��� Ŵ��� index �ա 1
                          // Ŵ��͹������Ҥ�� ���ҡ���Ҥ�� index array ���� 1
                          KMAT_keypadBuffer[--KMAT_keypadBuffer_index] = 0;
                        }

                        // add ��� key ������ buffer (function ���¢ͧ KMAT_multiple_readkey())
                        // keyadd = ����Ţ��������ͧ����� 1 �繤���á (��Ҩ����� index �ͧ array ��ͧź 1)
                        void KMAT_addkey_to_buffer( byte keyadd)
                        {
                          // ��һ����ҡ���� KMAT_ALL_KEYS ���� ���¡��� ���� = 0 ��� return ����ͧ save
                          if ( (keyadd > KMAT_ALL_KEYS) || (keyadd <= 0) ) return;
                          // ǹ�ٻ��Ǩ����բ����ū��������� ����ա���� add
                          byte loopChkExist = 0;
                          for (; loopChkExist < KMAT_KEYBUFFER_MAX; loopChkExist++)
                            if (KMAT_keypadBuffer[loopChkExist] == keyadd) return;
                          // ����Թ���Ҩ��Ѻ�� //�������ç������ж�� buffer ����Ҩ�������� loop
                          if (KMAT_keypadBuffer_index >= KMAT_KEYBUFFER_MAX) return;                           
                          // ����������� �֧ add key
                          KMAT_keypadBuffer[KMAT_keypadBuffer_index++] = keyadd;
                        }

                        // ���Ҥ�� key ����դ�ҷ���˹�� buffer ������� 
                        // (������鹷�� 1..n)
                        // return �繵��˹� index �ͧ array, �������¡��� 0 ��������
                        /*char KMAT_findKeyInBuff(byte keyValue)
                        {
                            byte index = 0;
                            for(; index < KMAT_KEYBUFFER_MAX; index++)
                                if (KMAT_keypadBuffer[index] == keyValue) 
                                    return index;
                            return -1;
                        }*/

                        // ��ҹ���Ẻ���»��� output �Т������������ KMAT_keypadBuffer[]
                        void KMAT_multiple_readkey()
                        {
                          KMAT_readkey(true);    
                          
                          // ����ͤú 1 �ͺ��ǹ loop ���ú�ء���� ���ͨд�����ջ����¡�����͡����Ǻ�ҧ
                          byte index = 0;
                          for (; index < KMAT_ALL_KEYS; index++)
                          {
                            // ����һ���㴡��� debounce �������� ����� add key
                            if (KMAT_timer_counter[index] >= KMAT_debounce_ms)
                            {
                              KMAT_addkey_to_buffer( index+1 );
                            }
                            // ����һ����¡����͡������
                            else if (KMAT_timer_counter[index] == 0)
                            {
                              // ������բ��������� buffer ������� ����ա�����͡�
                              byte y = 0;
                              for (; y < KMAT_KEYBUFFER_MAX; y++)
                              {
                                // ���Ң�������ҷ������顴�������� �����Ң������͡�
                                if (KMAT_keypadBuffer[y] == index+1) // ���Ф�� index �ͧ array ���¡��Ҥ�һ������� 1
                                  KMAT_remove_keybuffer(y);
                              }
                            }        
                          }      
                        }


                        // ��˹������� keymatrix
                        // arrRows = array ���Ţ�� rows Ẻ���§�ӴѺ
                        // arrCols = array ���Ţ�� cols Ẻ���§�ӴѺ
                        // debounce_ms = �������� (ms) ����Ѻ debounce
                        void KMAT_setup(byte arrRows[], byte arrCols[], byte debounce_ms)
                        {
                          byte count = 0;
                          // ��˹���� �� rows
                          for (count = 0; count < KMAT_ALL_ROWS; count++)
                          {
                            KMAT_leg_rows[count] = arrRows[count];
                            pinMode(arrRows[count],OUTPUT);
                            digitalWrite(arrRows[count], HIGH);
                          }
                          // ��˹���� �� columns
                          for (count = 0; count < KMAT_ALL_COLS; count++)
                          {
                            KMAT_leg_cols[count] = arrCols[count];
                            // �Դ internal r-pull up
                            pinMode(arrCols[count],OUTPUT);
                            digitalWrite(arrCols[count],HIGH);
                            // ����¹ mode
                            pinMode(arrCols[count],INPUT);    
                          }
                          
                          // init var
                          KMAT_debounce_ms = debounce_ms > 250 ? 250 : debounce_ms;
                          KMAT_global_ms = millis();
                          
                          // clear keybuffer
                          KMAT_clear_keybuffer();
                        }

                   
                        /*************************** End Keymatrix Part ****************************/

// ������һ����� ���� mouse buffer 
void addModifierMouseToBuffer(byte mouseCode)
{
    mouse_buffer[0] |= mouseCode;
}
// ������Ҥ������� buffer ���� key �����
void addModifierKeyToBuffer(byte keycode)
{
    keyboard_buffer.buffer[0] |= keycode;
}

// ������Ҥ�������� buffer ��㹡ó� KEYBOARD_BUFFER_SIZE ��ҹ��
void addKeycodeToBuffer(byte keycode)
{
    // ����Թ���Ҩ��Ѻ��
    if (keyboard_buffer.index >= KEYBOARD_BUFFER_SIZE) return;

    // ǹ�ٻ��Ǩ����բ����ū��������� ����ա�����ͧ add
    byte loopChkExist = 0;
    for (; loopChkExist < KEYBOARD_BUFFER_SIZE; loopChkExist++)
        if (keyboard_buffer.buffer[loopChkExist] == keycode) return;

    // ����������� �֧ add key
    keyboard_buffer.buffer[ keyboard_buffer.index++ ] = keycode;
}

// ��Ҥ�ҷ����� specialkey buffer �͡� ������Ѻ����͹���˹������
void removeSpecialKeyFromBuffer(byte index)
{
    byte count = index;
    for (; count < specialKeyIndex; count++)
    {
        special_buffer[count].buttonNo = special_buffer[count+1].buttonNo;
        special_buffer[count].address = special_buffer[count+1].address;
    }

    // �������Ѻ������ش���� Ŵ��� index ŧ 1 ��Т������ش������ 0
    special_buffer[--specialKeyIndex].buttonNo = 0;
    special_buffer[specialKeyIndex].address = 0;
}

// ������� keypad ����� special buffer 
// button = ����Ţ���� (������ҡ 1 �������)
// buttonAddr = ��� address �ͧ��������
void addSpecialKeyToBuffer(byte button, uint16_t buttonAddr)
{
    // ��Ǩ���� ������ӧҹ��ӡ�͹���ҷ���˹�
    if( millis() - specialKey_lasttime < SPECIAL_KEY_DEBOUNCE)
        return;
   
    // ǹ�ٻ��Ǩ����բ����ū��������� ����ա�����͡
    byte loopChkExist = 0;
    for (; loopChkExist < KEYBOARD_BUFFER_SIZE; loopChkExist++)
    {
        if (special_buffer[loopChkExist].buttonNo == button)
        {
            removeSpecialKeyFromBuffer(loopChkExist);
            //led_off();
            specialKey_lasttime = millis(); // ���͹حҵ�������ա���� ���е�ͧ debounce �͹��ԡ��
            return;
        }
    }
   
    // ����Թ���Ҩ��Ѻ�� �������鹵� ���ж�� key ����������� loop
    if (specialKeyIndex >= KEYBOARD_BUFFER_SIZE) 
    {
        //led_off();
        return;
    }

    // ����������� �֧ add key
    special_buffer[specialKeyIndex].buttonNo = button;
    special_buffer[specialKeyIndex++].address = buttonAddr;
    
    //led_on();
    specialKey_lasttime = millis();      // �ѹ�֡������� ��˹�ǧ
}

// ��Ҥ�ҷ����� specialkey buffer �͡� ������Ѻ����͹���˹������
// index ������ҡ 0
void removeMacroKeyFromBuffer(byte index)
{
    byte count = index;
    for (; count < macroKeyIndex; count++)
    {
        macro_buffer[count].buttonNo = macro_buffer[count+1].buttonNo;
        macro_buffer[count].address = macro_buffer[count+1].address;
        macro_buffer[count].delay = macro_buffer[count+1].delay;
    }

    // �������Ѻ������ش���� Ŵ��� index ŧ 1 ��Т������ش������ 0
    macro_buffer[--macroKeyIndex].buttonNo = 0;
    macro_buffer[macroKeyIndex].address = 0;
    macro_buffer[macroKeyIndex].delay = 0;
}

// ������� keypad ����� special buffer 
// button = ����Ţ���� (������ҡ 1 �������)
// buttonAddr = ��� address �ͧ��������
void addMacroKeyToBuffer(byte button, uint16_t buttonAddr)
{
    // ��Ǩ���� ������ӧҹ��ӡ�͹���ҷ���˹�
    if( millis() - macroKey_lasttime < MACRO_KEY_DEBOUNCE )
        return;

    // ǹ�ٻ��Ǩ����բ����ū��������� ����ա�����͡
    byte loopChkExist = 0;
    for (; loopChkExist < KEYBOARD_BUFFER_SIZE; loopChkExist++)
        if (macro_buffer[loopChkExist].buttonNo == button)
        {
            removeMacroKeyFromBuffer(loopChkExist);
            macroKey_lasttime = millis(); // ���͹حҵ�������ա���� ���е�ͧ debounce �͹��ԡ��
            return;
        }

    // ����Թ���Ҩ��Ѻ�� �������鹵����ж�� key ����С�Ѻ��¡��ԡ����� ���Ш������� loop
    if (macroKeyIndex >= KEYBOARD_BUFFER_SIZE) return;

    // ����������� �֧ add key
    macro_buffer[macroKeyIndex].buttonNo = button;
    macro_buffer[macroKeyIndex].address = buttonAddr;
    macro_buffer[macroKeyIndex++].delay = 0;

    macroKey_lasttime = millis();      // �ѹ�֡������� ��˹�ǧ
}

void clearKeyboardBuffer()
{
    byte count = 0;
    for(; count < KEYBOARD_BUFFER_SIZE; count++)
        keyboard_buffer.buffer[count] = 0;
    keyboard_buffer.index = 1; // ����� index 1 ���� index 0 �� modifier
}

void clearMouseBuffer()
{
    byte count = 0;
    for(; count < MOUSE_BUFFER_SIZE; count++)
        mouse_buffer[count] = 0;
}

// clear special key buff
void clearSpecialKeyBuffer()
{
    byte count = 0;
    for(; count < KEYBOARD_BUFFER_SIZE; count++)
    {
        special_buffer[count].buttonNo = 0;
        special_buffer[count].address = 0;
    }
    specialKeyIndex = 0;
    macro_interrupt_flag = true; // �������÷ӧҹẺ����� interrupt 
}

//
void clearMacroKeyBuffer()
{
    byte count = 0;
    for(; count < KEYBOARD_BUFFER_SIZE; count++)
    {
        macro_buffer[count].buttonNo = 0;
        macro_buffer[count].address  = 0;
        macro_buffer[count].delay    = 0;
    }
    macroKeyIndex = 0;
}

// �觤�� keyboard buffer � pc
// code �������� buffer 价�� buffer main program �����ѹ������ͧ�ա��
void sendKeyboardBuffer()
{
    byte count = 0;
    for(; count < KEYBOARD_BUFFER_SIZE; count++)
        inputBuffer1[count+1] = keyboard_buffer.buffer[count];

    // �͡�������ѡ���������觢�����
    tx_kbd = 1;
    
    clearKeyboardBuffer();             // ������ clear
    keyboard_lasttime = millis();      // �ѹ�֡������� ��˹�ǧ
    waitReleaseKeyboard = true;        // set ʶҹС�û���� Keyboard
}

// �������͡���觤�� mouse buffer � pc
// code �������� buffer 价�� buffer main program �����ѹ������ͧ�ա��
void sendMouseBuffer()
{
    byte count = 0;
    for(; count < MOUSE_BUFFER_SIZE; count++)
        inputBuffer2[count+1] = mouse_buffer[count];

    // �͡�������ѡ���������觢�����
    tx_mouse = 1;
    
    clearMouseBuffer();                 // ������ clear
    mouse_lasttime = millis();          // �ѹ�֡������� ��˹�ǧ
    waitReleaseMouse = true;            //set ʶҹС�û���� mouse
}

// �����ż� macrokey
// ���� address eeprom ���� ram �ҡ��������˹� 
// �� button ��������ҡ 1 
// ��Ѫ�� : ������ǹ�Դ��͡Ѻ��������� ����ѡ�ФԴ����ͧ �����á�� 1 ����
// �ѧ��鹶����ҧ�Ţ���� ��ͧ��������� 1 ���� ����� 0 �������顴
// return -1 ��������
// 㹡óչ����������鹵��˹觤Դ��Ҩҡ ram ����� index 0 ��Ҩ�����Ҩҡ eeprom �Ҩ�е�ͧ�� ���˹��������
//
// update ���ͧ�ҡ��� 50d ����ҡѺ 206d (���ǹ��Ѻ) �����䢷�����§��Ŵ�͡�ʡ�ê� �ѧ��鹨֧����� 
// �����Ը����� ��˹���� -50 ��� 206 (0xCE) �᷹
int16_t getAddressButton(byte button)
{
    if (button == 0) return -1; // ��һ��� 0 (����ը�ԧ) return ����������
    byte countButton = 1;
    uint16_t addr = EEPROM_ADDRESS_START;
    while ( (addr < MAX_ADDRESS_EEPROM) && (countButton < button) )
    {
        // ������Ũҡ EEProm ���觢����� 
        if ( eeprom_read_byte(addr) == END_BUTTON )
            countButton++;
        else if ( eeprom_read_byte(addr) == MACRO_NULL )
            break;
        addr++;
    }

    if( (addr < MAX_ADDRESS_EEPROM) && (button == countButton) )
        return addr; // ����� return address �ͧ button ����
    return -1;
}

// ��Ң����� �������� EEprom ���� RAM ���� ��� address ����˹�
// 㹷������� sample[]
// return 255(MACRO_NULL) �Ҩ�����¶֧�����ҧ����
byte getByteMacroCMD(uint16_t address)
{
    if(address < MAX_ADDRESS_EEPROM)
        return eeprom_read_byte(address); // ��ǧ���ͺ
    return MACRO_NULL;
}

// ��駤�� mouse XY ���¤���觹�� (��� process ��价� ��� main)
void mouseGotoXYSetup(int16_t buttonAddr)
{
    // ������������ ����� �� 6 byte ��� 
    // 1. ˹�Ҩ� x ��� 120 ��ɵ�ͧ + 1 (step ����͹᡹ x � 0) // ���ѧ�Ԩ�ó���Ҩ��� ���������
    // 2. ˹�Ҩ� y ��� 120 ��ɵ�ͧ + 1 (step ����͹᡹ y � 0) // ���ѧ�Ԩ�ó���Ҩ��� ���������
    // 3. target x (��ͧ��� 120 ��� + ���, ������繵��˹��ͺ�ش����)
    //      3.1 high byte
    //      3.2 low byte
    // 4. target y (��ͧ��� 120 ��� + ���, ������繵��˹��ͺ�ش����)
    //      4.1 high byte
    //      4.2 low byte
    directMouse.XStepReset = getByteMacroCMD(++buttonAddr); // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0
    directMouse.YStepReset = getByteMacroCMD(++buttonAddr); // �Ѻ�ͺ��ҷ��������Ƿ���������� 0,0
    uint16_t xt = getByteMacroCMD(++buttonAddr);            // ���˹� x ���·ҧ byte �٧
    xt <<= 8;
    xt |= getByteMacroCMD(++buttonAddr);                    // ���˹� x ���·ҧ byte ���
    uint16_t yt = getByteMacroCMD(++buttonAddr);            // ���˹� y ���·ҧ
    yt <<= 8;
    yt |= getByteMacroCMD(++buttonAddr);                    // ���˹� y ���·ҧ byte ���
    // process �ӹǹ�ͺ���˹� x,y, ���·ҧ
    directMouse.XStepTarget = xt/120;                       
    directMouse.YStepTarget = yt/120;
    directMouse.XTarget =  xt - (directMouse.XStepTarget*120);
    directMouse.YTarget =  yt - (directMouse.YStepTarget*120);
    // ����������÷ӧҹ
    directMouse.workingFlag = true;      
}

void KBCC_setup()
{
    // setup keyboard
    byte rowslegs[] = {5,4};         // ��˹��� Rows 
    byte colslegs[] = {3,2,1,0};     // ��˹��� Cols 
    KMAT_setup(rowslegs, colslegs, MAIN_DEBOUNCE);    // ���¡ setup �� delay 18 ms

    // initial variable
    sys_millis = 0;

    // reset all
    clearKeyboardBuffer();
    clearMouseBuffer();
    clearSpecialKeyBuffer();
    clearMacroKeyBuffer();
}


//
// ------------------- MacroMain Loop -------------------------------------------------------------
//
// sub function �����ҹ��� keypad ���������� ����红�����ŧ buffer
// ����繢����ŧ���� ������ö����ѹ�� ����� special key, macrokey ����ŧ buffer 
// ������ process special & macro key �Ѵ��õ�ҧ�ҡ
void sub_readKeypadAndProcess()
{
    // ��ҹ��� keypad
    KMAT_multiple_readkey();

    //clearKeyboardBuffer();  // �ӧҹ���� �ѹ����ͧ clear ���� clear �������

    // ����ա�á� keypad
    if(KMAT_keypadBuffer_index > 0)
    {
        // ǹ�ٻ�֧��Ң����Ţͧ������衴������
        byte countGetAllKeypad = 0;
        for ( ;countGetAllKeypad < KMAT_keypadBuffer_index; countGetAllKeypad++ )
        {
            // ����Ţ����仴������ address �������
            int16_t buttonAddr = getAddressButton( KMAT_keypadBuffer[countGetAllKeypad] );
            // ��Ҥ����� address �ͧ�������� (>=0)
            if (buttonAddr >= 0)
            {
                // ��ҹ�������ҷ��� 1 byte ������� �ٻẺ���ͤ��������
                byte dataRead = getByteMacroCMD(buttonAddr); // ��ҹ�����Ũҡ eeprom ���� ram ����

                // ����� ���� keyboard mapkey ������ �е�����¢������ա 2 bytes �������ʨ�(����Ǩ ���ͧ��)
                if (dataRead == MACRO_TYPE_KEYBOARD)
                {                    
                    addModifierKeyToBuffer( getByteMacroCMD(++buttonAddr) );
                    addKeycodeToBuffer( getByteMacroCMD(++buttonAddr) );
                }
                // ����� ���� mouse ������ �е�����¢������ա 3 bytes �������ʨ�(����Ǩ ���ͧ��)
                else if (dataRead == MACRO_TYPE_MOUSE)
                {
                    // ���� loop ������͡�� �֧��ͧ��Ǩ�����ͧ�ա���� ���蹹�鹨� + ��������Թ�
                    // ���˹�ǧ���Ҩ��� ���� repeat �ͧ mouse ����
                    if ((millis() - mouse_lasttime) > WAIT_MOUSE_REPEAT)
                    {
                        // ��ҹ�ա 3 byte
                        addModifierMouseToBuffer( getByteMacroCMD(++buttonAddr) );
                        mouse_buffer[1] += getByteMacroCMD(++buttonAddr);
                        mouse_buffer[2] += getByteMacroCMD(++buttonAddr);
                    }
                }
                // ����觡�˹����˹� mouse Ẻ XY �¡�è��ͧ mouse 仺��ش,�����ش (0,0)
                // ���� move mouse ���ú���˹�
                else if (dataRead == MACRO_TYPE_MOUSE_XY)
                {
                    mouseGotoXYSetup(buttonAddr);
                }
                // ����� special key (���Դ/���Ѻ) �е�����¢������ա 2 bytes
                else if (dataRead == MACRO_SPECIAL_KEY)
                {
                    // ��Ң������Ţ keypad �ѹ�֡������е�ͧ��Ѻ������͡ ����ա�á��������
                    // �ç��� add Keypad � buffer ���ҧ���� ���� ����Ͷ֧ loop process macro �Ф��Ҥ��
                    // ����§ŧ keybuffer �ա����(function �������������� �������͡��ҫ��)
                    // KMAT_keypadBuffer[countGetAllKeypad] = �Ţ������衴
                    // buttonAddr = address �˹��¤����Ӣͧ button
                    addSpecialKeyToBuffer( KMAT_keypadBuffer[countGetAllKeypad], buttonAddr );    
                }
                else if (dataRead == MACRO_BLOCK_START)
                {
                    // ��Ң������Ţ keypad �ѹ�֡������е�ͧ��Ѻ������͡ ����ա�á��������
                    // �ç��� add Keypad � buffer ���ҧ���� ���� ����Ͷ֧ loop process macro �Ф��Ҥ��
                    // ����§ŧ keybuffer �ա����  (function �������������� �������͡��ҫ��)
                    // KMAT_keypadBuffer[countGetAllKeypad] = �Ţ������衴
                    // buttonAddr = address �˹��¤����Ӣͧ button
                    addMacroKeyToBuffer( KMAT_keypadBuffer[countGetAllKeypad], buttonAddr );
                }
            } // ����Ǩ��������ӴѺ keypad ������ address �ͧ����
        } // �� loop �����㹡�ô֧�����Ţͧ�����ء������衴
    }
}

// Sub function ������ҹ��� specialkey � buffer ���ǨѴ���§ŧ keyboard buffer
void sub_specialKeyProcess()
{
    // ��Ǩ SpecialKey ����ա�á�������� ����� ���§������ŧ buffer ������� ��ҵԴ interrupt flag
    if ( (specialKeyIndex > 0) && (!macro_interrupt_flag) )
    {
        // ǹ�ٻ�֧��Ң����� ŧ����� keyboard buffer (�»��� ����繢����ū�� �������������������������)
        byte count = 0;
        for (;count < specialKeyIndex; count++)
        {
            // ��ҹ�����Ũҡ eeprom ���� ram ���� +1 ��� byte modifers; +2 ��� keycode
            // 㹷�����ͧ�� 2 byte �ҡ�ش��� ����繤���� special key
            byte dataRead = getByteMacroCMD( special_buffer[count].address+1 );
            addModifierKeyToBuffer( dataRead );
            dataRead = getByteMacroCMD( special_buffer[count].address+2 );
            addKeycodeToBuffer( dataRead );
        }
    }
}

// ��ǹ��èѴ��� macrokey buffer ���ǨѴ���§ŧ keyboard & mouse buffer
void sub_macroKeyProcess()
{
    // ����բ����� macro � buffer
    if ( macroKeyIndex > 0 )
    {
        // ǹ�ٻ�֧��Ң����� ŧ����� keyboard buffer (�»��� ����繢����ū�� �������������������������)
        byte count = 0;
        for (;count < macroKeyIndex; count++)
        {
            // ��ҹ�����Ũҡ eeprom ���� ram ���� ������ҡ����觵��� (���+1 �ҡ������)
            byte dataRead = 0;
            byte cmdExit = false;
            // ������ block ���� MACRO_BLOCK_STOP
            while ( (!cmdExit) &&
                    (dataRead != MACRO_BLOCK_STOP) && 
                    (macro_buffer[count].address < MAX_ADDRESS_EEPROM) )
            {
                // ��һ������Դ loop delay ��Ŵ��ҵ������ delay
                if ( macro_buffer[count].delay > 0 )
                {
                    // ź���ҷ���ҹ� ��ҵԴ delay
                    // �������ҷ���ҹ�.. ����Ҩ���Դ�ҡ mouse ���� key ���� delay ���� 
                    if (millis() > macro_global_time) // ��ͧ�ѹ��÷ӧҹ�����Թ���� 1ms
                    {
                        uint16_t passedTime = (uint16_t)(millis() - macro_global_time);
                        // ��ͧ�ѹ���ź�����Թ仵Դź (��������յԴź)
                        if (macro_buffer[count].delay < passedTime)
                            macro_buffer[count].delay = 0;
                        else
                            macro_buffer[count].delay -= passedTime;

                        // �ѹ�֡�����ա���� ���ͺ����
                        macro_global_time = millis();
                    }
                    // ���ͺ˹��
                    cmdExit = true;
                }
                else
                {
                    // ��һ�����������Դ loop delay ��ӧҹ���� ��...
                    // ��ҹ 1 byte �Ѵ� �Ҵ٤����
                    dataRead = getByteMacroCMD( ++macro_buffer[count].address );            
                    switch(dataRead)
                    {
                        /*case MACRO_BLOCK_START:
                        {
                            // ��ҹ�
                            break;
                        }*/
                        //����� mouse
                        case MACRO_CMD_MOUSE:
                        {
                            // ��ҹ������� 3 byte
                            // ��ҹ byte �á �� modifier mouse
                            dataRead = getByteMacroCMD( ++macro_buffer[count].address );
                            addModifierMouseToBuffer( dataRead );
                            // 2 byte ������繢�����
                            mouse_buffer[1] = getByteMacroCMD( ++macro_buffer[count].address );
                            mouse_buffer[2] = getByteMacroCMD( ++macro_buffer[count].address );

                            // �� block ����ͧ delay ���ҧ���� = ��� REPEAT
                            macro_buffer[count].delay += WAIT_MOUSE_END_MACRO_BLOCK;
                            // �纤������
                            macro_global_time = millis();
                            break;
                        }
                        case MACRO_CMD_MOUSE_XY:
                        {
                            // ����觡�˹����˹� mouse ����͹�Ѻ MACRO_TYPE_MOUSE_XY ����ҹ��� macro
                            mouseGotoXYSetup( macro_buffer[count].address );
                            // ��ͧ���� address ���� 6 byte
                            macro_buffer[count].address += 6;
                            break;
                        }
                        case MACRO_CMD_KEYBOARD:
                        {
                            // ��ҹ byte �á �� modifier key
                            byte modifiers = getByteMacroCMD( ++macro_buffer[count].address );
                            addModifierKeyToBuffer( modifiers );
                            // ��ҹ������� ����Թ 6 byte ��ͨҡ��� ���ͨ����Ҩ��� 0
                            byte cmdExit2 = false;
                            byte countIndex = 1; //(������ҹ����� 1 byte)
                            while ((!cmdExit2) && (countIndex < KEYBOARD_BUFFER_SIZE))
                            {
                                // ��ҹ���������ա 1 byte �������� �����Ҩ������� 0 
                                byte dataRead = getByteMacroCMD( ++macro_buffer[count].address );
                                if (dataRead != 0)
                                {
                                    /* ��ǹ���ͧ�ѹ�����衴 numlock ����� work �����Ҥ�� numlock status �����
                                    // ����繵���Ţ�·��
                                    if (dataRead >= KEY_NUM_1 && dataRead <= KEY_NUM_0 && !IsNumlockOn)
                                    {
                                        // ��� keybuffer �ѹ�á�բ����� , ��Ҥ�� keyboard �ѹ�á���� buffer �ش����
                                        if( keyboard_buffer.buffer[1] > 0)
                                        {
                                            // ��Ѻ����� byte �á��ͧ������ �Դ numlock
                                            addKeycodeToBuffer( keyboard_buffer.buffer[1] );
                                            keyboard_buffer.buffer[1] = KEY_NUM_LOCK; // �Դ numlock
                                        }
                                        else
                                        {
                                            // �������բ������� ��������
                                            addKeycodeToBuffer( KEY_NUM_LOCK );
                                        }
                                    }*/
                                    addKeycodeToBuffer(dataRead);
                                    countIndex++;
                                }
                                else
                                {
                                    cmdExit2 = true;
                                }
                            }
                            // �� block ����ͧ delay ���ҧ���� = ��� REPEAT
                            macro_buffer[count].delay += WAIT_KEYBOARD_END_MACRO_BLOCK;
                            // �纤������
                            // *** ����¹����Ẻ��� �����ա�����ҡ�˹����� ������纤������ ����ͧ��Ǩ (�����Ѵ flash �ҡ����)
                            macro_global_time = millis();
                            break;
                        }                        
                        case MACRO_CMD_INTERRUPT:
                        {
                            // ��ش�觢����� 1 �ѧ��� �����ŷ�������� keyboard ��ҹ�� 2 byte
                            // set flag interrupt
                            macro_interrupt_flag = true;
                            clearKeyboardBuffer();
                            addModifierKeyToBuffer( getByteMacroCMD(++macro_buffer[count].address) );
                            addKeycodeToBuffer( getByteMacroCMD(++macro_buffer[count].address) );
                            // �� block ����ͧ delay ���ҧ���� = ��� REPEAT
                            macro_buffer[count].delay += WAIT_KEYBOARD_END_MACRO_BLOCK;
                            // �纤������
                            // *** ����¹����Ẻ��� �����ա�����ҡ�˹����� ������纤������ ����ͧ��Ǩ (�����Ѵ flash �ҡ����)
                            macro_global_time = millis();
                            break;
                        }
                        case MACRO_CMD_DISABLE_ALL:
                        {
                            // save old data
                            byte old_bt = macro_buffer[count].buttonNo;
                            uint16_t old_addr = macro_buffer[count].address +1;  // ���� �� block ��ҹ�� �����Ҩ���դ���� �����Ҩ�Ш�������¡���

                            // ¡��ԡ����觢����ŷ����� ��� clear all buffer
                            clearKeyboardBuffer();
                            clearMouseBuffer();
                            clearSpecialKeyBuffer();
                            clearMacroKeyBuffer();
                            
                            // ��ѧ�ҡ clear ���������ǡ�����һ������ŧ� �������������������еԴ����ͧ����
                            // ��ͧ���Ը� add ŧ仵ç� 
                            macro_buffer[macroKeyIndex].buttonNo = old_bt;
                            macro_buffer[macroKeyIndex++].address = old_addr;

                            // �͡�ҡ loop
                            cmdExit = true;
                            // remove ����� index ����¹ ���������������� (��� count ����դ���ҡ)
                            count = 255;
                            break;
                        }
                        case MACRO_CMD_DELAY:
                        {
                            // ��ҹ�����ŵ���ա 2 byte ����������Ѻ���� ��ͨҡ�ͧ���
                            // 㹡óշ���ͧ���Ŵ������� delay �ͧ������ ���繤��ź (-)
                            // ��ҹ��� byte �٧
                            int16_t timedelay = 0;
                            timedelay = getByteMacroCMD( ++macro_buffer[count].address );
                            timedelay <<= 8;
                            // ��ҹ��� byte ���
                            timedelay |= getByteMacroCMD( ++macro_buffer[count].address );
                            macro_buffer[count].delay += timedelay;
                            break;
                        }
                        case MACRO_BLOCK_STOP:
                        {
                            // �� block ����ͧ delay ���ҧ���� = ��� REPEAT
                            macro_buffer[count].delay += WAIT_END_MACRO_BLOCK;
                            // �� 1 block ��ش�͡�ҡ while 任������� ���ͺ˹��
                            cmdExit = true;
                            break;
                        }
                        // �ҡ��ͧ�������Ѻ������ MACRO �����ա����
                        // ��ͧ���ͺ
                        case MACRO_CMD_REPEAT:
                        {
                            // ��ҹ�����ŵ������ʹ���Ҩ������͹��Ѻ��ѧ���˹觷���ͧ���������
                            dataRead = getByteMacroCMD( ++macro_buffer[count].address );
                            macro_buffer[count].address -= dataRead;
                            // �� 1 block ��ش�͡�ҡ while 任������� ���ͺ˹��
                            cmdExit = true;
                            break;
                        }
                        case END_BUTTON:
                        {
                            // ��һ��� macro ����͡�ҡ buffer
                            removeMacroKeyFromBuffer( count ); // �Ҩҡ�ͺ�ͧ��õ�Ǩ�Ѻ macrokey
                            // �͡�ҡ while
                            cmdExit = true;
                            // remove ����� index ����¹ ���������������� (��� count ����դ���ҡ
                            count = 255;
                            break;
                        }
                    } // �� switch
                    
                    // ��ҵ�Ǩ��������һ�����ա�����Ѻ���� delay 
                    // *** ����¹���Ẻ����ͷ����ա�����ҡ�˹����� ������纤������ ����ͧ��Ǩ (�����Ѵ flash �ҡ����)
                    /*if (macro_buffer[count].delay > 0)
                    {
                        // �������������ҵ���診���� block ������� loop ���ź����
                        macro_global_time = millis();
                    }*/
                }// �� if else [check delay]
            }
        }
    } 
}

// Sub function Send Mouse ��� Keyboard Buffer ��Ҥú���� ������բ����ŷ���ͧ��
// ��ͧ�ѹ�������觢����Ŷ���Թ�
void sub_sendMouseAndKeyBuffer()
{
    //
    // ����� delay �������������ѧ
    if ( (millis() - mouse_lasttime) > WAIT_MOUSE_REPEAT)
    {                       
        // ��Ǩ������ա������¹�ŧ� mouse_buffer ������� ����ա���
        if (mouse_buffer[0] != 0 || mouse_buffer[1] != 0 || mouse_buffer[2] != 0)
        {
            sendMouseBuffer();          // �� mouse buffer
        } 
    }
    // ˹�ǧ���� (ms) ���¡�õ�Ǩ ��ͧ�ѹ�觢����� keyboard ����Թ�
    if ( (millis() - keyboard_lasttime) > WAIT_KEYBOARD_REPEAT)
    {
        // ��Ǩ������ա�á�������� keyboard ������� (���ҧ���� 1 ����)
        if(keyboard_buffer.buffer[0] != 0 || keyboard_buffer.buffer[1] != 0)
        {
            sendKeyboardBuffer();       // �� Keyboard buffer
            //macro_interrupt_flag = false;  //��������͡�ٻ
        }
        macro_interrupt_flag = false;
    }

    // ----- Block Release ----------------------------------------------------------------------------------
    // �������ա�á� key ���� mouse ��� release (��������)
    if ( waitReleaseMouse && millis()-mouse_lasttime > WAIT_MOUSE_RELEASED) 
    {
      clearMouseBuffer();
      sendMouseBuffer();
      waitReleaseMouse = false;
    }
    if ( waitReleaseKeyboard && millis()-keyboard_lasttime > WAIT_KEYBOARD_RELEASED)
    {
      clearKeyboardBuffer();
      sendKeyboardBuffer();
      waitReleaseKeyboard = false;
    }
}

void KBCC_loop()
{
    //
    // ----- Block �Ѵ��� SpecialKey buffer ���§������ŧ buffer �����͡���� -----------------------------------
    // �������������ѹ�á������Ҩ�������żŤ����衴��ҧ����͹ (Priority �٧����)
    sub_specialKeyProcess();
    //
    // ----- Block �����ҹ��� keypad ���������� ����红�����ŧ buffer ------------------------------------------
    //
    sub_readKeypadAndProcess();
    //
    // ----- Block ��Ǩ MacroKey ����ա�á�������� ����� ���§������ŧ buffer (�����¹Դ˹���) ------------------------
    //
    sub_macroKeyProcess(); 
    //
    // ----- Block Send Mouse and Keyboard Buffer ��Ҥú���� ����բ����ŷ���ͧ�� --------------------------------
    // �� process �ش����
    sub_sendMouseAndKeyBuffer();
}
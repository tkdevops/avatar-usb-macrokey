License
=======

The project is built with AVR USB driver by Objective Development, which is
published under a proprietary Open Source license. To conform with this
license, InfraHID is distributed under the same license conditions. See the
file "firmware/usbdrv/License.txt" for details.

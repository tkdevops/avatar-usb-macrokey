# Avatar-USB-Macrokey
ซอร์สโค้ดและ schematic ของผลิตภัณฑ์ "Avatar Macro Key" โดยรุ่นนี้สามารถเขียน macro script เพื่อ emulate ได้ทั้ง keyboard/mouse ในเวลาเดียวกัน

<img src="images/AVATAR-new-design.png" width="403" height="360"><br />


# ที่มา
เกม DotA มีระบบป้องกันการใช้ software macro key แต่การเล่นเกมนี้ผ่าน keyboard ของ notebook เป็นความยุ่งยากมากๆ และยิ่งถ้าเปลี่ยน Hero <br /> ก็จะต้องเปลี่ยนปุ่มกด Skill ไปด้วยก็ยิ่งยุ่งยากไปอีก <br />

<img src="images/Dota_Artwork.jpg" width="403" height="360"><br /> <br />

ผู้พัฒนาได้เห็นความยุ่งยาก จึงได้เริ่มพัฒนา macro keyboard ด้วย Microcontoller ATMEGA32 เพื่อช่วยให้การกด skill ต่าง ๆ เป็นไปได้โดยง่าย <br />
โดยก่อนหน้านี้ ผู้พัฒนาได้เคยทดลองทำรุ่น PS/2 มาก่อนแล้ว ก็ปรากฏว่าสามารถทำงานได้เป็นอย่างดี เมื่อคุยกับเพื่อนในกลุ่ม ต่างชื่นชอบ <br /> ในรุ่นนี้ผู้จัดทำจึงพัฒนาต่อยอดขึ้นไปอีก โดยปรับเปลี่ยน interface เป็น USB อีกทั้งยังสามารถ Emulate ได้ทั้ง mouse และ keyboard <br />
ได้ในเวลาเดียวกัน นอกจากนี้ยังรองรับการเขียน Macro Script  จึงทำให้สามารถจัดหนัก skill ได้แบบชุดใหญ่ไฟกระพริบ <br />
เช่น `Rhasta` สามารถกดเพียง `ปุ่มเดียว` ก็ฆ่าโรฮาน และวาร์ปหนีกลับบ้านได้ทันที :)

![one-click-one-kill](video/rhasta-1click1kill-04052552-0120.mp4)

# หลักการทำงาน
AVATAR macrokey  จะรับข้อมูล script ผ่าน serial port เพียงเปิดโปรแกรม `avatar_setup.exe` และใส่ชุดคำสั่ง macrokey ตามคู่มือ <br /> 
คุณสามารถใส่เรียงลำดับได้ตามจำนวนปุ่มที่ต้องการ   ชุดคำสั่งสามารถควบคุมได้ทั้ง mouse และ keyboard และสามารถหน่วงเวลาการทำงานได้

# new design
<img src="images/avatar-orange.jpg" width="448" height="336"><br />
<img src="images/SDC10615-avatar-uploader-sm.jpg" width="448" height="336"><br />
<img src="images/SDC10643-sm.jpg" width="448" height="336"><br />

# สรุป
ในแวดวงคนเล่นเกม DotA ต่างก็ชื่นชอบ พอๆ กับชุดแสดงผล Font ภาษาไทยในเกม DotA ที่ผู้จัดทำได้เคยสร้างไปก่อนหน้านี้ <br />
จึงสามารถจัดจำหน่ายได้หลายร้อยชุด สร้างกำไรได้พอสมควร :) <br />
<img src="images/SDC10771-sm.JPG" width="448" height="336"><br />


`TKVR`


